from django.shortcuts import render, redirect, get_object_or_404
from django.db import connection, transaction, IntegrityError
from green.forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required
from django.urls import reverse

# Create your views here.
def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        email = request.session.get('username')
        if email is None:
            return redirect('/login/')
        else:
            return function(request, *args, **kwargs)
    return wrapper


def cursor_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


@user_login_required
def create_transaksi_makan(request):
    if request.session.get("role") != 'ADMIN_SATGAS' or request.session.get("role") == 'PENGGUNA_PUBLIK':
        return redirect('/')
    if request.method == 'POST':
        form = CreateTransaksiMakan(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/list_transaksi_makan/")
    if request.method == 'GET':
        form = CreateTransaksiMakan()
    context = {'form': form}
    return render(request, "CreateTransaksiMakan.html", context)


@user_login_required
def list_transaksi_makan(request):
    if request.session.get("role") == 'ADMIN_SATGAS' or request.session.get("role") == 'PENGGUNA_PUBLIK':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_MAKAN")
            hasil = cursor_fetchall(cursor)
        context = {'data' : hasil}
    else:
        return redirect('/')
    return render(request, "ListTransaksiMakan.html", context)


@user_login_required
def detail_transaksi_makan(request):
    if request.session.get("role") == "ADMIN_SATGAS":
        idtransaksimakan = request.GET.get('idtransaksimakan')
        if idtransaksimakan is None:
            return redirect('/list_transaksi_makan/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM TRANSAKSI_MAKAN")

            cursor.execute("SELECT * FROM PAKET_MAKAN WHERE kodepaket=%s", [idtransaksimakan])

            cursor.execute("SELECT PM.kodepaket, PM.harga, TM.totalbayar FROM PAKET_MAKAN PM JOIN DAFTAR_PESAN DP ON DP.kodepaket = PM.kodepaket JOIN TRANSAKSI_MAKAN TM ON TM.idtransaksimakan = DP.idtransaksimakan GROUP BY PM.kodepaket, PM.harga, TM.totalbayar", [idtransaksimakan])
            
            # q1 = cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_MAKAN")
            # q2 = cursor.execute("SELECT PM.kodepaket, PM.harga, TM.totalbayar FROM SIRUCO.PAKET_MAKAN PM JOIN DAFTAR_PESAN DP ON DP.kodepaket = PM.kodepaket JOIN TRANSAKSI_MAKAN TM ON TM.idtransaksimakan = DP.idtransaksimakan GROUP BY PM.kodepaket, PM.harga, TM.totalbayar", [transaksi_makan])
            # queries = [q1, q2]
            # results = cursor.execute(";".join(queries), multi=True)
            hasil = cursor_fetchall(cursor)
        context = {'data' : hasil}
    else:
        return redirect('/')
    return render(request, "DetailTransaksiMakan.html", context)


@user_login_required
def update_transaksi_makan(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = UpdateTransaksiMakan(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/list_transaksi_makan/')
    elif request.method == 'GET':
        idtransaksimakan = request.GET.get('idtransaksimakan')
        if idtransaksimakan is None:
            return redirect('/list_transaksi_makan/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_MAKAN WHERE NOT EXISTS (SELECT statusbayar FROM SIRUCO.TRANSAKSI_HOTEL WHERE statusbayar='Lunas')", [idtransaksimakan])
            hasil = cursor_fetchall(cursor)
        context = {'form': form}
        return render(request, "UpdateTransaksiMakan.html", context)
    

@user_login_required
def delete_transaksi_makan(request):
    if request.method == "GET":
        idtransaksimakan = request.GET.get('idtransaksimakan')
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM SIRUCO.TRANSAKSI_MAKAN WHERE idtransaksimakan=%s", [idtransaksimakan])
    return redirect('/list_transaksi_makan')


@user_login_required
def create_paket_makan(request):
    if request.session.get("role") != 'ADMIN':
        return redirect('/')
    if request.method == 'POST':
        form = CreateTransaksiMakan(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/list_paket_makan/")
    if request.method == 'GET':
        form = CreatePaketMakan()
    context = {'form': form}
    return render(request, "CreatePaketMakan.html", context)


@user_login_required
def list_paket_makan(request):
    if request.session.get("role") == 'ADMIN_SATGAS' or request.session.get("role") == 'PENGGUNA_PUBLIK' or request.session.get("role") == 'ADMIN':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.PAKET_MAKAN")
            hasil = cursor_fetchall(cursor)
        context = {'data' : hasil}
    else:
        return redirect('/')
    return render(request, "ListPaketMakan.html", context)


@user_login_required
def update_paket_makan(request):
    if request.session.get("role") != "ADMIN":
        return redirect('/ListPaketMakan')
    
    if request.method == "GET":
        kodehotel = request.GET.get('kodehotel')
        kodepaket = request.GET.get('kodepaket')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.PAKET_MAKAN", [kodehotel,kodepaket])
            val = cursor.fetchone()
            print(val)
            key = [col[0] for col in cursor.description]
            print(key)
            data = dict(zip(key, val))
        form = UpdatePaketMakan(initial=data)
    elif request.method == 'POST':
        form = UpdatePaketMakan(request.POST)
        if form.is_valid():
            form.save_update()
            return redirect('/ListPaketMakan')
    context = {'form':form}
    return render(request,'UpdatePaketMakan.html',context) 


@user_login_required
def delete_paket_makan(request):
    if request.method == "GET":
        kodehotel = request.GET.get('kodehotel')
        kodepaket = request.GET.get('kodepaket')
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM SIRUCO.PAKET_MAKAN WHERE kodehotel=%s and kodepaket=%s", [kodehotel, kodepaket])
    return redirect('/list_paket_makan')


# @user_login_required
# def create_hotel(request):


@user_login_required
def list_hotel(request):
    if request.session.get("role") == 'ADMIN_SATGAS' or request.session.get("role") == 'PENGGUNA_PUBLIK' or request.session.get("role") == 'ADMIN':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.HOTEL")
            cursor.execute("SELECT kode, nama, isrujukan, CONCAT(jalan, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', prov) AS alamat FROM SIRUCO.HOTEL")
            hasil = cursor_fetchall(cursor)
        context = {'data' : hasil}
    else:
        return redirect('/')
    return render(request, "ListHotel.html", context)

@user_login_required
def update_hotel(request):
    if request.session.get("role") == 'ADMIN_SATGAS' or request.session.get("role") == 'PENGGUNA_PUBLIK' or request.session.get("role") == 'ADMIN':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.HOTEL")
            hasil = cursor_fetchall(cursor)
        context = {'data' : hasil}
    else:
        return redirect('/')
    return render(request, "UpdateHotel.html", context)
