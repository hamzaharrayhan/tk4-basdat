from django.urls import path
from . import views

app_name = 'green'

urlpatterns = [
    path('create_transaksi_makan/', views.create_transaksi_makan, name='create_transaksi_makan'),
    path('list_transaksi_makan/', views.list_transaksi_makan, name='list_transaksi_makan'),
    path('update_transaksi_makan/', views.update_transaksi_makan, name='update_transaksi_makan'),
    path('delete_transaksi_makan/', views.delete_transaksi_makan, name='delete_transaksi_makan'),
    path('detail_transaksi_makan/', views.detail_transaksi_makan, name='detail_transaksi_makan'),

    path('create_paket_makan/', views.create_paket_makan, name='create_paket_makan'),
    path('list_paket_makan/', views.list_paket_makan, name='list_paket_makan'),
    path('update_paket_makan/', views.update_paket_makan, name='update_paket_makan'),
    path('delete_paket_makan/', views.delete_paket_makan, name='delete_paket_makan'),

    # path('create_hotel/', views.create_hotel, name='create_hotel'),
    path('list_hotel/', views.list_hotel, name='list_hotel'),
    path('update_hotel/', views.update_hotel, name='update_hotel'),
]