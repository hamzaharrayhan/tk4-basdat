--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: siruco; Type: SCHEMA; Schema: -; Owner: db202b04
--

CREATE SCHEMA siruco;


ALTER SCHEMA siruco OWNER TO db202b04;

--
-- Name: bed_amount(); Type: FUNCTION; Schema: siruco; Owner: db202b04
--

CREATE FUNCTION siruco.bed_amount() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

IF (TG_NAME = 'bed_in') THEN
UPDATE RUANGAN_RS R 
SET JmlBed = JmlBed + 1
WHERE R.KodeRuangan = NEW.KodeRuangan AND R.KodeRS = NEW.KodeRS;

ELSEIF (TG_NAME = 'reservasi_in') THEN
UPDATE RUANGAN_RS R 
SET JmlBed = JmlBed - 1
WHERE R.KodeRuangan = NEW.KodeRuangan AND R.KodeRS = NEW.KodeRS;

RETURN NEW;
END IF;
RETURN NULL;

EXCEPTION
WHEN SQLState '23514' THEN 
RAISE EXCEPTION 'Jumlah bed pada ruangan yang dipesan sedang kosong.';
RETURN NULL;

END;
$$;


ALTER FUNCTION siruco.bed_amount() OWNER TO db202b04;

--
-- Name: check_jml_pasien(); Type: FUNCTION; Schema: siruco; Owner: db202b04
--

CREATE FUNCTION siruco.check_jml_pasien() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
jumlahPasienTemp smallint;

BEGIN
SELECT JmlPasien into jumlahPasienTemp
FROM JADWAL_DOKTER JD, MEMERIKSA M
WHERE
JD.Username = M.Username_Dokter AND 
JD.NoSTR = M.NoSTR AND 
JD.Kode_Faskes = M.Kode_Faskes AND 
JD.Shift = M.Praktek_Shift AND 
JD.Tanggal = M.Praktek_Tgl AND
JD.Username = New.Username_Dokter AND 
JD.NoSTR = New.NoSTR AND 
JD.Kode_Faskes = New.Kode_Faskes AND 
JD.Shift = New.Praktek_Shift;

IF (jumlahPasienTemp >= 30)
THEN
RAISE EXCEPTION 'Jadwal appointment dengan dokter % sudah penuh.', New.Username_Dokter;

ELSE
UPDATE JADWAL_DOKTER JD
SET JmlPasien = JmlPasien + 1
WHERE 
JD.Username = NEW.Username_Dokter AND
JD.NoSTR = NEW.NoSTR AND JD.Kode_Faskes = NEW.Kode_Faskes
AND JD.Shift = NEW.Praktek_Shift AND JD.Tanggal = NEW.Praktek_Tgl;
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION siruco.check_jml_pasien() OWNER TO db202b04;

--
-- Name: check_password(); Type: FUNCTION; Schema: siruco; Owner: db202b04
--

CREATE FUNCTION siruco.check_password() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN IF 
NEW.password SIMILAR TO '%[A-Z]%' AND
NEW.password SIMILAR TO '%[0-9]%' THEN
RETURN NEW;
ELSE
RAISE EXCEPTION 'Password untuk username % harus mengandung minimal 1 huruf kapital dan 1 angka', NEW.username;
RETURN NULL;
END IF;
END;
$$;


ALTER FUNCTION siruco.check_password() OWNER TO db202b04;

--
-- Name: create_transaksi_rs(); Type: FUNCTION; Schema: siruco; Owner: db202b04
--

CREATE FUNCTION siruco.create_transaksi_rs() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO TRANSAKSI_RS 
VALUES(CONCAT ('TRS',nextval('id_transaksi_rs')), new.KodePasien, NULL, NULL, new.Tglmasuk, ((NEW.tglkeluar::date - NEW.tglmasuk::date)*500000), 'Belum Lunas');
RETURN NULL;
END;
$$;


ALTER FUNCTION siruco.create_transaksi_rs() OWNER TO db202b04;

--
-- Name: rsvhotel(); Type: FUNCTION; Schema: siruco; Owner: db202b04
--

CREATE FUNCTION siruco.rsvhotel() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 declare
 hargaKamar integer;
 jumlahHari integer;
 BEGIN
 SELECT (NEW.tglkeluar::date - NEW.tglmasuk::date) into jumlahHari;
 select harga into hargaKamar from hotel_room where new.kodehotel = hotel_room.kodehotel
 and new.koderoom = hotel_room.koderoom;
 insert into transaksi_hotel(kodepasien,statusbayar,totalbayar)
values (new.kodepasien,'Belum Lunas',hargaKamar*jumlahHari);
 insert into transaksi_booking(totalbayar,kodepasien,tglmasuk)
values(hargaKamar*jumlahHari,new.kodepasien,new.tglmasuk);
 return new;
 END;
$$;


ALTER FUNCTION siruco.rsvhotel() OWNER TO db202b04;

--
-- Name: total_bayar_makan(); Type: FUNCTION; Schema: siruco; Owner: db202b04
--

CREATE FUNCTION siruco.total_bayar_makan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
totalBayarMakan integer;
totalTransaksiMakan integer;
totalBiayaBooking integer;

BEGIN 
SELECT SUM(PM.Harga) into totalBayarMakan
FROM TRANSAKSI_MAKAN TM, DAFTAR_PESAN DP, PAKET_MAKAN PM
WHERE
TM.IdTransaksi = DP.idtransaksi AND TM.IdTransaksiMakan = DP.IdTransaksiMakan AND
PM.KodeHotel = DP.KodeHotel AND PM.KodePaket = DP.KodePaket AND
TM.IdTransaksi = NEW.idtransaksi AND TM.IdTransaksiMakan = NEW.IdTransaksiMakan
GROUP BY TM.IdTransaksi, TM.IdTransaksiMakan;

UPDATE TRANSAKSI_MAKAN TM
SET TotalBayar = totalBayarMakan
WHERE TM.idTransaksi = New.idTransaksi AND 
TM.idTransaksimakan = New.idTransaksimakan;

SELECT TotalBayar into totalTransaksiMakan 
FROM TRANSAKSI_MAKAN TM
WHERE TM.idTransaksi = New.idTransaksi AND 
TM.idTransaksimakan = New.idTransaksimakan;

SELECT TotalBayar into totalBiayaBooking
FROM TRANSAKSI_BOOKING TB
WHERE TB.idTransaksibooking = New.IdTransaksi;

UPDATE TRANSAKSI_HOTEL TH 
SET TotalBayar = totalBiayaBooking +  totalTransaksiMakan
WHERE 
TH.idTransaksi = NEW.idTransaksi;
RETURN NULL;
END;
$$;


ALTER FUNCTION siruco.total_bayar_makan() OWNER TO db202b04;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.admin (
    username character varying(50) NOT NULL
);


ALTER TABLE siruco.admin OWNER TO db202b04;

--
-- Name: admin_satgas; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.admin_satgas (
    username character varying(50) NOT NULL,
    idfaskes character varying(3) NOT NULL
);


ALTER TABLE siruco.admin_satgas OWNER TO db202b04;

--
-- Name: akun_pengguna; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.akun_pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    peran character varying(20) NOT NULL
);


ALTER TABLE siruco.akun_pengguna OWNER TO db202b04;

--
-- Name: bed_rs; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.bed_rs (
    koderuangan character varying(5) NOT NULL,
    koders character varying(3) NOT NULL,
    kodebed character varying(5) NOT NULL
);


ALTER TABLE siruco.bed_rs OWNER TO db202b04;

--
-- Name: daftar_pesan; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.daftar_pesan (
    idtransaksimakan character varying(10) NOT NULL,
    idtransaksi character varying(10) NOT NULL,
    id_pesanan integer NOT NULL,
    kodehotel character varying(5),
    kodepaket character varying(5)
);


ALTER TABLE siruco.daftar_pesan OWNER TO db202b04;

--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE; Schema: siruco; Owner: db202b04
--

CREATE SEQUENCE siruco.daftar_pesan_id_pesanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE siruco.daftar_pesan_id_pesanan_seq OWNER TO db202b04;

--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE OWNED BY; Schema: siruco; Owner: db202b04
--

ALTER SEQUENCE siruco.daftar_pesan_id_pesanan_seq OWNED BY siruco.daftar_pesan.id_pesanan;


--
-- Name: dokter; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.dokter (
    username character varying(50) NOT NULL,
    nostr character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    nohp character varying(12) NOT NULL,
    gelardepan character varying(10) NOT NULL,
    gelarbelakang character varying(10) NOT NULL
);


ALTER TABLE siruco.dokter OWNER TO db202b04;

--
-- Name: faskes; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.faskes (
    kode character varying(3) NOT NULL,
    tipe character varying(30) NOT NULL,
    nama character varying(50) NOT NULL,
    statusmilik character varying(30) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.faskes OWNER TO db202b04;

--
-- Name: gedung; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.gedung (
    kodegedung character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.gedung OWNER TO db202b04;

--
-- Name: gejala_pasien; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.gejala_pasien (
    nik character varying(20) NOT NULL,
    namagejala character varying(50) NOT NULL
);


ALTER TABLE siruco.gejala_pasien OWNER TO db202b04;

--
-- Name: hotel; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.hotel (
    kode character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.hotel OWNER TO db202b04;

--
-- Name: hotel_room; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.hotel_room (
    kodehotel character varying(5) NOT NULL,
    koderoom character varying(5) NOT NULL,
    jenisbed character varying(10) NOT NULL,
    tipe character varying(10) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.hotel_room OWNER TO db202b04;

--
-- Name: id_transaksi_rs; Type: SEQUENCE; Schema: siruco; Owner: db202b04
--

CREATE SEQUENCE siruco.id_transaksi_rs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE siruco.id_transaksi_rs OWNER TO db202b04;

--
-- Name: id_trs_hotel; Type: SEQUENCE; Schema: siruco; Owner: db202b04
--

CREATE SEQUENCE siruco.id_trs_hotel
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE siruco.id_trs_hotel OWNER TO db202b04;

--
-- Name: jadwal; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.jadwal (
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE siruco.jadwal OWNER TO db202b04;

--
-- Name: jadwal_dokter; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.jadwal_dokter (
    nostr character varying(20) NOT NULL,
    username character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL,
    jmlpasien integer
);


ALTER TABLE siruco.jadwal_dokter OWNER TO db202b04;

--
-- Name: kamar_rumah; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.kamar_rumah (
    koderumah character varying(5) NOT NULL,
    kodekamar character varying(5) NOT NULL,
    jenisbed character varying(20) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.kamar_rumah OWNER TO db202b04;

--
-- Name: komorbid_pasien; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.komorbid_pasien (
    nik character varying(20) NOT NULL,
    namakomorbid character varying(50) NOT NULL
);


ALTER TABLE siruco.komorbid_pasien OWNER TO db202b04;

--
-- Name: memeriksa; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.memeriksa (
    nik_pasien character varying(20) NOT NULL,
    nostr character varying(20) NOT NULL,
    username_dokter character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    praktek_shift character varying(15) NOT NULL,
    praktek_tgl date NOT NULL,
    rekomendasi character varying(500)
);


ALTER TABLE siruco.memeriksa OWNER TO db202b04;

--
-- Name: paket_makan; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.paket_makan (
    kodehotel character varying(5) NOT NULL,
    kodepaket character varying(5) NOT NULL,
    nama character varying(20) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.paket_makan OWNER TO db202b04;

--
-- Name: pasien; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.pasien (
    nik character varying(20) NOT NULL,
    idpendaftar character varying(50),
    nama character varying(50) NOT NULL,
    ktp_jalan character varying(30) NOT NULL,
    ktp_kelurahan character varying(30) NOT NULL,
    ktp_kecamatan character varying(30) NOT NULL,
    ktp_kabkot character varying(30) NOT NULL,
    ktp_prov character varying(30) NOT NULL,
    dom_jalan character varying(30) NOT NULL,
    dom_kelurahan character varying(30) NOT NULL,
    dom_kecamatan character varying(30) NOT NULL,
    dom_kabkot character varying(30) NOT NULL,
    dom_prov character varying(30) NOT NULL,
    notelp character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL
);


ALTER TABLE siruco.pasien OWNER TO db202b04;

--
-- Name: pengguna_publik; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.pengguna_publik (
    username character varying(50) NOT NULL,
    nik character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    status character varying(10) NOT NULL,
    peran character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL
);


ALTER TABLE siruco.pengguna_publik OWNER TO db202b04;

--
-- Name: reservasi_gedung; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.reservasi_gedung (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodegedung character varying(5),
    noruang character varying(5),
    notempattidur character varying(5)
);


ALTER TABLE siruco.reservasi_gedung OWNER TO db202b04;

--
-- Name: reservasi_hotel; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.reservasi_hotel (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodehotel character varying(5),
    koderoom character varying(5)
);


ALTER TABLE siruco.reservasi_hotel OWNER TO db202b04;

--
-- Name: reservasi_rs; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.reservasi_rs (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koders character varying(3),
    koderuangan character varying(5),
    kodebed character varying(5),
    kodeventilator character varying(5)
);


ALTER TABLE siruco.reservasi_rs OWNER TO db202b04;

--
-- Name: reservasi_rumah; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.reservasi_rumah (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koderumah character varying(5),
    kodekamar character varying(5)
);


ALTER TABLE siruco.reservasi_rumah OWNER TO db202b04;

--
-- Name: ruangan_gedung; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.ruangan_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    namaruangan character varying(20) NOT NULL
);


ALTER TABLE siruco.ruangan_gedung OWNER TO db202b04;

--
-- Name: ruangan_rs; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.ruangan_rs (
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    tipe character varying(10) NOT NULL,
    jmlbed integer NOT NULL,
    harga integer NOT NULL,
    CONSTRAINT jmlbed_non_zero CHECK ((jmlbed >= 0))
);


ALTER TABLE siruco.ruangan_rs OWNER TO db202b04;

--
-- Name: rumah; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.rumah (
    koderumah character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.rumah OWNER TO db202b04;

--
-- Name: rumah_sakit; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.rumah_sakit (
    kode_faskes character varying(3) NOT NULL,
    isrujukan character(1) NOT NULL
);


ALTER TABLE siruco.rumah_sakit OWNER TO db202b04;

--
-- Name: telepon_faskes; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.telepon_faskes (
    kode_faskes character varying(3) NOT NULL,
    notelp character varying(20) NOT NULL
);


ALTER TABLE siruco.telepon_faskes OWNER TO db202b04;

--
-- Name: tempat_tidur_gedung; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.tempat_tidur_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    notempattidur character varying(5) NOT NULL
);


ALTER TABLE siruco.tempat_tidur_gedung OWNER TO db202b04;

--
-- Name: tes; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.tes (
    nik_pasien character varying(20) NOT NULL,
    tanggaltes date NOT NULL,
    jenis character varying(10) NOT NULL,
    status character varying(15) NOT NULL,
    nilaict character varying(5) NOT NULL
);


ALTER TABLE siruco.tes OWNER TO db202b04;

--
-- Name: totalbayarmakan; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.totalbayarmakan (
    idtransaksi character varying(10),
    idtransaksimakan character varying(10),
    sum bigint
);


ALTER TABLE siruco.totalbayarmakan OWNER TO db202b04;

--
-- Name: transaksi_booking; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.transaksi_booking (
    idtransaksibooking character varying(10) DEFAULT to_char(currval('siruco.id_trs_hotel'::regclass), '"trsH"fm000'::text) NOT NULL,
    totalbayar integer,
    kodepasien character varying(20),
    tglmasuk date
);


ALTER TABLE siruco.transaksi_booking OWNER TO db202b04;

--
-- Name: transaksi_hotel; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.transaksi_hotel (
    idtransaksi character varying(10) DEFAULT to_char(nextval('siruco.id_trs_hotel'::regclass), '"trsH"fm000'::text) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL
);


ALTER TABLE siruco.transaksi_hotel OWNER TO db202b04;

--
-- Name: transaksi_makan; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.transaksi_makan (
    idtransaksi character varying(10) NOT NULL,
    idtransaksimakan character varying(10) NOT NULL,
    totalbayar integer
);


ALTER TABLE siruco.transaksi_makan OWNER TO db202b04;

--
-- Name: transaksi_rs; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.transaksi_rs (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    tglmasuk date,
    totalbiaya integer,
    statusbayar character varying(15) NOT NULL
);


ALTER TABLE siruco.transaksi_rs OWNER TO db202b04;

--
-- Name: transaksi_rumah; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.transaksi_rumah (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL,
    tglmasuk date
);


ALTER TABLE siruco.transaksi_rumah OWNER TO db202b04;

--
-- Name: ventilator; Type: TABLE; Schema: siruco; Owner: db202b04
--

CREATE TABLE siruco.ventilator (
    koders character varying(3) NOT NULL,
    kodeventilator character varying(5) NOT NULL,
    kondisi character varying(10) NOT NULL
);


ALTER TABLE siruco.ventilator OWNER TO db202b04;

--
-- Name: daftar_pesan id_pesanan; Type: DEFAULT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.daftar_pesan ALTER COLUMN id_pesanan SET DEFAULT nextval('siruco.daftar_pesan_id_pesanan_seq'::regclass);


--
-- Data for Name: admin; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.admin (username) FROM stdin;
cteml8
dainsby1
tarnaudet2
gdouthwaite3
lblinder4
mswayland5
jjeacock6
medgett7
ihoudmont9
vwogdena
aborgb
novidc
mfishbied
jvosee
abrientf
sebbeng
kraccioh
mgageri
awingattj
eabramcik0
\.


--
-- Data for Name: admin_satgas; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.admin_satgas (username, idfaskes) FROM stdin;
abrientf	fk1
sebbeng	fk2
kraccioh	fk3
mgageri	fk4
awingattj	fk5
mswayland5	fk1
jjeacock6	fk2
novidc	fk3
mfishbied	fk7
jvosee	fk6
\.


--
-- Data for Name: akun_pengguna; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.akun_pengguna (username, password, peran) FROM stdin;
eabramcik0	1wFIhxccui	Admin
dainsby1	1Wco91l	Admin
tarnaudet2	1Ohv5d9Su	Admin
gdouthwaite3	17myUJI4i7y	Admin
lblinder4	1yvEjzd	Admin
mswayland5	1CwdKbUJ	Admin Satgas
jjeacock6	126DnD80O2dyf	Admin Satgas
medgett7	1o22LMaOZ	Dokter
cteml8	135qzyuoI30MQ	Dokter
ihoudmont9	1YU3Tv1UjpYq	Dokter
vwogdena	1Gys6xw	Dokter
aborgb	14OG7QmH8	Dokter
novidc	17JiP9ZtfjRAq	Admin Satgas
mfishbied	1vO4WZE1Z	Admin Satgas
jvosee	1d6LdhzeO	Admin Satgas
abrientf	1B2uHBTzZ9D	Admin Satgas
sebbeng	10703AC	Admin Satgas
kraccioh	1Fke9UhhAyGI	Admin Satgas
mgageri	1jrJDIu	Admin Satgas
awingattj	1Qhju8oLWzTfy	Admin Satgas
tconahyk	1lN01FLZeWSGo	Pasien
rdelahayl	1rqEd69G	Pasien
ehowsamm	1DPEUqN3l	Pasien
amccaheyn	1OyUdZdz	Pasien
stipenso	1GZa64SBP	Pasien
\.


--
-- Data for Name: bed_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.bed_rs (koderuangan, koders, kodebed) FROM stdin;
R1fk1	fk1	B1Fk1
R1fk1	fk1	B2Fk1
R1fk1	fk1	B3Fk1
R1fk2	fk2	B1Fk2
R1fk3	fk3	B1Fk3
R1fk3	fk3	B2Fk3
R1fk4	fk4	B1Fk4
R1fk5	fk5	B1Fk5
R2fk5	fk5	B1Fk5
R2fk5	fk5	B2Fk5
\.


--
-- Data for Name: daftar_pesan; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.daftar_pesan (idtransaksimakan, idtransaksi, id_pesanan, kodehotel, kodepaket) FROM stdin;
trsM001	trsH001	1	H1	p1h1
trsM002	trsH002	2	H2	p2h2
trsM003	trsH003	3	H3	p1h3
trsM004	trsH004	4	H4	p2h4
trsM005	trsH005	5	H5	p1h5
\.


--
-- Data for Name: dokter; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.dokter (username, nostr, nama, nohp, gelardepan, gelarbelakang) FROM stdin;
medgett7	3874625193738203	Kort	83434343435	dr.	SpA(K)
cteml8	3857294729375023	Madalena	86546732828	dr.	M.Kes
ihoudmont9	3874639572957295	Melicent	83627382943	Prof. Dr.	phd
vwogdena	2848593857493057	Eddy	81387779203	dr.	SpOG(K)
aborgb	3334445556667778	Aviva	87654790774	Prof. Dr.	M.Kes
\.


--
-- Data for Name: faskes; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.faskes (kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
fk1	Rumah Sakit	RS Bunda	Pemerintah	Jalan Manggis	Cimanggis	Bogor Utara	Bogor	Jawa Barat
fk2	Rumah Sakit	Hermina	Swasta	Jalan Apel	Pondok Cina	Bekasi Utara	Bekasi	Jawa Barat
fk3	Rumah Sakit	Harapan Bunda	Swasta	Jalan Melati	Pondok Cina	Jakarta Selatan	Jakarta	Jakarta
fk4	Rumah Sakit	BMC	Pemerintah	Jalan Anggur	Lenteng Agung	Jakarta Selatan	Jakarta	Jakarta
fk5	Rumah Sakit	UI	Pemerintah	Jalan Buntu	Benhil	Sentul	Bogor	Jawa Barat
fk6	Puskesmas	Yasmin	Pemerintah	Jalan Burung	Benhil	Jakarta Selatan	Jakarta	Jakarta
fk7	Klinik	Jiwa Sehat	Swasta	Jalan Jalan	Benhil	Sentul	Bogor	Jawa Barat
\.


--
-- Data for Name: gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.gedung (kodegedung, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
Gd1	Wisma Atlet	1	Jalan Graha	Sunter Agung	Pademangan	Jakarta Utara	DKI Jakarta
Gd2	Rumah Lawan Covid-19	1	Jalan Bumi	Pondok Cina	Beji	Depok	Jawa Barat
Gd3	Pusdiklat Kemendagri	1	Jalan Pakualam	Lenteng Agung	Jakarta Selatan	Jakarta	DKI Jakarta
Gd4	Panti Sosial Bina Remaja	0	Jalan Tomampe	Lere	Palu Bar	Palu	Sulawesi Tengah
Gd5	Wisma Jakarta Islamic Center	0	Jalan Wayang	Benhil	Jakarta Selatan	Jakarta	DKI Jakarta
\.


--
-- Data for Name: gejala_pasien; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.gejala_pasien (nik, namagejala) FROM stdin;
3610658371	Sakit Kepala
3610658371	Ruam di kulit
8515693742	Batuk ringan
8515693742	Demam tinggi
2003870733	Demam tinggi
4606832922	Batuk ringan
6453057271	Ruam di kulit
5748397400	Sakit kepala
6103584796	Sesak nafas
6103584796	Lemas
6281178494	Batuk ringan
6281178494	Sesak nafas
\.


--
-- Data for Name: hotel; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.hotel (kode, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
H1	Hotel 1	1	Jl. Borobudur	Cimanggis	Bogor Utara	Bogor	Jawa Barat
H2	Hotel 2	1	Jl. Pajajaran	Pondok Cina	Bekasi Utara	Bekasi	Jawa Barat
H3	Hotel 3	0	Jl. Djuanda	Pondok Cina	Jakarta Selatan	Jakarta	Jakarta
H4	Hotel 4	0	Jl. Majapahit	Lenteng Agung	Jakarta Selatan	Jakarta	Jakarta
H5	Hotel 5	0	Jl. Pramuka	Benhil	Sentul	Bogor	Jawa Barat
\.


--
-- Data for Name: hotel_room; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.hotel_room (kodehotel, koderoom, jenisbed, tipe, harga) FROM stdin;
H1	R1H1	King	deluxe	850000
H2	R1H2	TwinSingle	premium	2000000
H3	R1H3	TwinSingle	suite	1500000
H4	R1H4	King	deluxe	750000
H5	R1H5	King	premium	2000000
H1	R2H1	TwinSingle	deluxe	750000
H2	R2H2	King	premium	2000000
H3	R2H3	TwinSingle	suite	1500000
H4	R2H4	King	deluxe	750000
H5	R2H5	TwinSingle	premium	2000000
\.


--
-- Data for Name: jadwal; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.jadwal (kode_faskes, shift, tanggal) FROM stdin;
fk1	shift-1	2020-07-07
fk2	shift-2	2020-08-12
fk3	shift-3	2020-09-11
fk4	shift-2	2020-10-09
fk5	shift-3	2020-09-04
fk1	shift-4	2020-09-09
fk2	shift-5	2020-10-01
fk3	shift-2	2020-07-08
fk4	shift-3	2020-08-10
fk5	shift-1	2020-11-03
fk6	shift-3	2020-12-19
fk7	shift-1	2020-12-17
\.


--
-- Data for Name: jadwal_dokter; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.jadwal_dokter (nostr, username, kode_faskes, shift, tanggal, jmlpasien) FROM stdin;
3874625193738203	medgett7	fk1	shift-1	2020-07-07	20
3857294729375023	cteml8	fk2	shift-2	2020-08-12	12
3874639572957295	ihoudmont9	fk3	shift-3	2020-09-11	12
2848593857493057	vwogdena	fk4	shift-2	2020-10-09	23
3334445556667778	aborgb	fk5	shift-3	2020-09-04	23
3874639572957295	ihoudmont9	fk1	shift-4	2020-09-09	12
3334445556667778	aborgb	fk7	shift-1	2020-12-17	10
3874639572957295	ihoudmont9	fk6	shift-3	2020-12-19	12
2848593857493057	vwogdena	fk2	shift-5	2020-10-01	30
\.


--
-- Data for Name: kamar_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.kamar_rumah (koderumah, kodekamar, jenisbed, harga) FROM stdin;
Rm1	K1Rm1	Triple	120000
Rm2	K1Rm2	Twin	150000
Rm3	K1Rm3	Queen	300000
Rm4	K1Rm4	Triple	300000
Rm5	K1Rm5	Twin	300000
Rm6	K1Rm6	Queen	290000
Rm1	K2Rm1	Queen	350000
Rm2	K2Rm2	Twin	300000
Rm3	K2Rm3	Triple	200000
Rm4	K2Rm4	Queen	300000
Rm5	K2Rm5	Twin	240000
Rm6	K2Rm6	Triple	150000
\.


--
-- Data for Name: komorbid_pasien; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.komorbid_pasien (nik, namakomorbid) FROM stdin;
3610658371	Asma
3610658371	Diabetes Melitus
2003870733	Hipertensi
4606832922	Diabetes
6453057271	Penyakit Ginjal
5570803702	Tumor
5570803702	Gagal Jantung
5925991098	Stroke
6103584796	Asma
6103584796	Tuberkulosis
\.


--
-- Data for Name: memeriksa; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.memeriksa (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl, rekomendasi) FROM stdin;
3610658371	3874625193738203	medgett7	fk1	shift-1	2020-07-07	butuh ventilator
8515693742	3857294729375023	cteml8	fk2	shift-2	2020-08-12	hanya isolasi mandiri
2003870733	3874639572957295	ihoudmont9	fk3	shift-3	2020-09-11	membutuhkan ICU
4606832922	2848593857493057	vwogdena	fk4	shift-2	2020-10-09	rawat inap
6453057271	3334445556667778	aborgb	fk5	shift-3	2020-09-04	butuh ventilator
5570803702	3874639572957295	ihoudmont9	fk1	shift-4	2020-09-09	butuh ventilator
5748397400	2848593857493057	vwogdena	fk2	shift-5	2020-10-01	hanya isolasi mandiri
5925991098	3334445556667778	aborgb	fk5	shift-3	2020-09-04	membutuhkan ICU
6103584796	3874639572957295	ihoudmont9	fk1	shift-4	2020-09-09	rawat inap
6281178494	2848593857493057	vwogdena	fk2	shift-5	2020-10-01	butuh ventilator
\.


--
-- Data for Name: paket_makan; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.paket_makan (kodehotel, kodepaket, nama, harga) FROM stdin;
H1	p1h1	Ikan bandeng	20000
H1	p2h1	Bubur Manado	35000
H2	p1h2	Daging	40000
H2	p2h2	Bubur Cianjur	30000
H3	p1h3	Ayam Cincang	20000
H3	p2h3	Daging rendang	35000
H4	p1h4	Sayur Bayam	15000
H4	p2h4	Ayam Bakar	35000
H5	p1h5	Buah	25000
H5	p2h5	Pudding Coklat	13000
\.


--
-- Data for Name: pasien; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.pasien (nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp) FROM stdin;
3610658371	tconahyk	Ailyn	Jalan Manggis	Benhil	Bogor Utara	Bogor	Jawa Barat	Jalan Manggis	Benhil	Bogor Utara	Bogor	Jawa Barat	610-839-1685	46347131284
8515693742	rdelahayl	Amy	Jalan Apel	Pondok Cina	Bekasi Utara	Bekasi	Jawa Barat	Jalan Apel	Pondok Cina	Bekasi Utara	Bekasi	Jawa Barat	518-350-1329	41795676523
2003870733	ehowsamm	Stan	Jalan Melati	Pondok Cina	Jakarta Selatan	Jakarta	Jawa Barat	Jalan Melati	Pondok Cina	Jakarta Selatan	Jakarta	Jakarta	612-511-0218	45646453902
4606832922	amccaheyn	Hamzah	Jalan Anggur	Lenteng Agung	Jakarta Selatan	Jakarta	Jawa Barat	Jalan Anggur	Lenteng Agung	Jakarta Selatan	Jakarta	Jakarta	250-291-5193	32343222121
6453057271	stipenso	Ayuka	Jalan Buntu	Cimanggis	Sentul	Bogor	Jawa Barat	Jalan Buntu	Cimanggis	Sentul	Bogor	Jawa Barat	941-348-9542	32197367828
5570803702	tconahyk	Nisa	Jalan Mekar	Bintaro	Jakarta Selatan	Jakarta	Jawa Barat	Jalan Mekar	Bintaro	Jakarta Selatan	Jakarta	Jakarta	392-635-5851	45673920203
5748397400	rdelahayl	Rizky	Jalan Raya	Curug Mekar	Tanah Sareal	Bogor	Jawa Barat	Jalan Raya	Curug Mekar	Tanah Sareal	Bogor	Jawa Barat	698-424-4749	54234243223
5925991098	ehowsamm	Atta	Jalan Pinang	Kukusan	Beji	Depok	Jawa Barat	Jalan Samping	Pesanggrahan	Tegal Gundil	Tegal	Jawa Tengah	929-411-9666	49876543246
6103584796	amccaheyn	Aurel	Jalan Lily	Himalaya	Bekasi Utara	Bekasi	Jawa Barat	Jalan Soekarno	Kebayoran	Kembang Ayu	Yogyakarta	Jawa Tengah	911-778-8958	23242432244
6281178494	stipenso	Sabyan	Jalan Anggrek	Putrajaya	Tangerang Selatan	Tangerang	Jawa Barat	Jalan Hatta	Cipete	Depok Lama	Depok	Jawa Barat	874-687-4611	33344455556
\.


--
-- Data for Name: pengguna_publik; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.pengguna_publik (username, nik, nama, status, peran, nohp) FROM stdin;
tconahyk	3610658371	Ailyn	AKTIF	Pasien	46347131284
rdelahayl	8515693742	Amy	AKTIF	Pasien	41795676523
ehowsamm	2003870733	Stan	NON AKTIF	Pasien	45646453902
amccaheyn	4606832922	Mocca	NON AKTIF	Pasien	30969972682
stipenso	6453057271	Sunny	AKTIF	Pasien	57927342759
\.


--
-- Data for Name: reservasi_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.reservasi_gedung (kodepasien, tglmasuk, tglkeluar, kodegedung, noruang, notempattidur) FROM stdin;
3610658371	2020-06-22	2020-07-06	Gd1	R1Gd1	Bed1
8515693742	2020-06-23	2020-07-07	Gd2	R1Gd2	Bed1
2003870733	2020-07-27	2020-08-10	Gd3	R1Gd3	Bed1
4606832922	2020-08-12	2020-08-26	Gd4	R1Gd4	Bed1
6453057271	2020-08-21	2020-09-04	Gd5	R1Gd5	Bed1
5570803702	2020-09-03	2020-09-17	Gd1	R2Gd1	Bed2
5748397400	2020-09-07	2020-09-21	Gd2	R2Gd2	Bed2
5925991098	2020-09-08	2020-09-22	Gd3	R2Gd3	Bed2
6103584796	2020-11-27	2020-12-11	Gd4	R2Gd4	Bed2
6281178494	2020-12-25	2021-01-08	Gd5	R2Gd5	Bed2
\.


--
-- Data for Name: reservasi_hotel; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.reservasi_hotel (kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom) FROM stdin;
5570803702	2020-09-12	2020-09-26	H1	R1H1
5748397400	2020-10-02	2020-10-16	H2	R1H2
5925991098	2020-09-11	2020-09-25	H3	R1H3
6103584796	2020-09-12	2020-09-26	H4	R1H4
6281178494	2020-10-04	2020-10-18	H5	R1H5
\.


--
-- Data for Name: reservasi_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.reservasi_rs (kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed, kodeventilator) FROM stdin;
3610658371	2020-07-08	2020-07-15	fk1	R1fk1	B1Fk1	V1fk1
8515693742	2020-08-13	2020-08-21	fk2	R1fk2	B1Fk2	\N
2003870733	2020-09-12	2020-09-20	fk3	R1fk3	B1Fk3	\N
4606832922	2020-10-10	2020-10-17	fk4	R1fk4	B1Fk4	\N
6453057271	2020-09-05	2020-09-15	fk5	R1fk5	B1Fk5	V1fk5
5925991098	2020-09-04	2020-09-17	fk5	R2fk5	B2Fk5	\N
\.


--
-- Data for Name: reservasi_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.reservasi_rumah (kodepasien, tglmasuk, tglkeluar, koderumah, kodekamar) FROM stdin;
6453057271	2020-07-08	2020-07-22	Rm1	K1Rm1
5570803702	2020-08-13	2020-08-27	Rm2	K1Rm2
5748397400	2020-09-12	2020-09-26	Rm3	K1Rm3
5925991098	2020-10-10	2020-10-24	Rm4	K1Rm4
6103584796	2020-09-05	2020-09-19	Rm5	K1Rm5
6281178494	2020-09-04	2020-09-18	Rm6	K1Rm6
4606832922	2020-12-01	2020-12-15	Rm6	K2Rm6
\.


--
-- Data for Name: ruangan_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.ruangan_gedung (kodegedung, koderuangan, namaruangan) FROM stdin;
Gd1	R1Gd1	Matoa
Gd2	R1Gd2	Akasia
Gd3	R1Gd3	Pinus
Gd4	R1Gd4	Kenari
Gd5	R1Gd5	Aren
Gd1	R2Gd1	Karet
Gd2	R2Gd2	Cemara
Gd3	R2Gd3	Eboni
Gd4	R2Gd4	Beringin
Gd5	R2Gd5	Cengkeh
Gd1	R3Gd1	Matahari
Gd2	R3Gd2	Anggrek
Gd3	R3Gd3	Bulan
Gd4	R3Gd4	Jahe
Gd5	R3Gd5	Anggur
\.


--
-- Data for Name: ruangan_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.ruangan_rs (koders, koderuangan, tipe, jmlbed, harga) FROM stdin;
fk1	R1fk1	Kelas 3	3	500000
fk2	R1fk2	VIP	1	500000
fk3	R1fk3	Kelas 2	2	500000
fk4	R1fk4	ICU	1	500000
fk5	R1fk5	VIP	1	500000
fk5	R2fk5	Kelas 1	2	500000
\.


--
-- Data for Name: rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.rumah (koderumah, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
Rm1	Rumah Mawar	1	Jalan Graha	Pondok Cina	Bekasi Utara	Bekasi	Jawa Barat
Rm2	Rumah Lili	1	Jalan Bumi	Pondok Cina	Jakarta Selatan	Jakarta	Jawa Barat
Rm3	Rumah Anggrek	0	Jalan Pakualam	Lenteng Agung	Jakarta Selatan	Jakarta	Jakarta
Rm4	Rumah Teratai	1	Jalan Gunung	Benhil	Sentul	Bogor	Jakarta
Rm5	Rumah Melati	0	Jalan Wayang	Benhil	Jakarta Selatan	Jakarta	Jawa Barat
Rm6	Rumah Tulip	0	Jalan Pisang	Benhil	Sentul	Bogor	Jakarta
\.


--
-- Data for Name: rumah_sakit; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.rumah_sakit (kode_faskes, isrujukan) FROM stdin;
fk1	1
fk2	0
fk3	1
fk4	1
fk5	0
\.


--
-- Data for Name: telepon_faskes; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.telepon_faskes (kode_faskes, notelp) FROM stdin;
fk1	5673999888
fk2	5873829834
fk3	5675433223
fk4	5393929223
fk5	5678912345
fk6	5827373292
fk7	5738282992
\.


--
-- Data for Name: tempat_tidur_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.tempat_tidur_gedung (kodegedung, koderuangan, notempattidur) FROM stdin;
Gd1	R1Gd1	Bed1
Gd2	R1Gd2	Bed1
Gd3	R1Gd3	Bed1
Gd4	R1Gd4	Bed1
Gd5	R1Gd5	Bed1
Gd1	R2Gd1	Bed2
Gd2	R2Gd2	Bed2
Gd3	R2Gd3	Bed2
Gd4	R2Gd4	Bed2
Gd5	R2Gd5	Bed2
Gd1	R3Gd1	Bed3
Gd2	R3Gd2	Bed3
Gd3	R3Gd3	Bed3
Gd4	R3Gd4	Bed3
Gd5	R3Gd5	Bed3
\.


--
-- Data for Name: tes; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.tes (nik_pasien, tanggaltes, jenis, status, nilaict) FROM stdin;
3610658371	2020-02-05	PCR	Reaktif	29
8515693742	2020-06-09	Antigen	Non-Reaktif	38
2003870733	2020-08-12	Antigen	Reaktif	26
4606832922	2020-09-04	PCR	Reaktif	26
4606832922	2020-10-04	Antigen	Non-Reaktif	37
6453057271	2020-09-04	PCR	Reaktif	28
5570803702	2020-09-06	Antigen	Reaktif	27
5748397400	2020-11-08	Antigen	Non-Reaktif	38
5748397400	2020-10-08	Antigen	Reaktif	27
5925991098	2020-10-24	Antigen	Non-Reaktif	36
6103584796	2020-12-28	Antigen	Reaktif	26
6103584796	2021-01-25	Antigen	Non-Reaktif	37
6281178494	2020-01-30	PCR	Non-Reaktif	37
\.


--
-- Data for Name: totalbayarmakan; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.totalbayarmakan (idtransaksi, idtransaksimakan, sum) FROM stdin;
trsH001	trsM001	1050000
trsH002	trsM002	1400000
trsH003	trsM003	1304000
trsH004	trsM004	1536000
trsH005	trsM005	700000
\.


--
-- Data for Name: transaksi_booking; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.transaksi_booking (idtransaksibooking, totalbayar, kodepasien, tglmasuk) FROM stdin;
trsH001	11900000	5570803702	2020-09-12
trsH002	28000000	5748397400	2020-10-02
trsH003	21000000	5925991098	2020-09-11
trsH004	10500000	6103584796	2020-09-12
trsH005	28000000	6281178494	2020-10-04
\.


--
-- Data for Name: transaksi_hotel; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.transaksi_hotel (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar) FROM stdin;
trsH001	5570803702	\N	\N	11920000	Belum Lunas
trsH002	5748397400	\N	\N	28030000	Belum Lunas
trsH003	5925991098	\N	\N	21020000	Belum Lunas
trsH004	6103584796	\N	\N	10535000	Belum Lunas
trsH005	6281178494	\N	\N	28025000	Belum Lunas
\.


--
-- Data for Name: transaksi_makan; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.transaksi_makan (idtransaksi, idtransaksimakan, totalbayar) FROM stdin;
trsH001	trsM001	20000
trsH002	trsM002	30000
trsH003	trsM003	20000
trsH004	trsM004	35000
trsH005	trsM005	25000
\.


--
-- Data for Name: transaksi_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.transaksi_rs (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, tglmasuk, totalbiaya, statusbayar) FROM stdin;
TRS1	3610658371	2020-07-15	2020-07-15 07:30:40	2020-07-08	3500000	paid
TRS2	8515693742	2020-08-21	2020-08-21 08:12:22	2020-08-13	4000000	paid
TRS3	2003870733	2020-09-20	2020-09-20 13:00:00	2020-09-12	4000000	paid
TRS4	4606832922	2020-10-17	2020-10-17 12:08:50	2020-10-10	3500000	paid
TRS5	6453057271	2020-09-15	2020-09-15 08:09:04	2020-09-05	5000000	paid
TRS6	5925991098	2020-09-17	2020-09-17 16:40:04	2020-09-04	6500000	paid
\.


--
-- Data for Name: transaksi_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.transaksi_rumah (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar, tglmasuk) FROM stdin;
TRSR001	6453057271	2020-07-08	2020-07-08 13:34:22	1680000	Paid	2020-07-08
TRSR002	5570803702	2020-08-13	2020-08-13 11:09:08	2100000	Paid	2020-08-13
TRSR003	5748397400	2020-09-12	2020-09-12 16:40:22	4200000	Partial	2020-09-12
TRSR004	5925991098	2020-10-10	2020-10-10 09:40:22	4200000	Partial	2020-10-10
TRSR005	6103584796	2020-09-05	2020-09-05 08:07:25	4200000	Paid	2020-09-05
TRSR006	6281178494	2020-09-04	2020-09-04 14:06:07	4060000	Paid	2020-09-04
TRSR007	4606832922	2020-12-01	2020-12-01 15:22:22	2100000	Paid	2020-12-01
\.


--
-- Data for Name: ventilator; Type: TABLE DATA; Schema: siruco; Owner: db202b04
--

COPY siruco.ventilator (koders, kodeventilator, kondisi) FROM stdin;
fk1	V1fk1	rusak
fk2	V1fk2	berfungsi
fk3	V1fk3	berfungsi
fk4	V1fk4	berfungsi
fk5	V1fk5	berfungsi
fk5	V2fk5	berfungsi
\.


--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE SET; Schema: siruco; Owner: db202b04
--

SELECT pg_catalog.setval('siruco.daftar_pesan_id_pesanan_seq', 1, false);


--
-- Name: id_transaksi_rs; Type: SEQUENCE SET; Schema: siruco; Owner: db202b04
--

SELECT pg_catalog.setval('siruco.id_transaksi_rs', 7, false);


--
-- Name: id_trs_hotel; Type: SEQUENCE SET; Schema: siruco; Owner: db202b04
--

SELECT pg_catalog.setval('siruco.id_trs_hotel', 6, true);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (username);


--
-- Name: admin_satgas admin_satgas_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_pkey PRIMARY KEY (username);


--
-- Name: akun_pengguna akun_pengguna_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.akun_pengguna
    ADD CONSTRAINT akun_pengguna_pkey PRIMARY KEY (username);


--
-- Name: bed_rs bed_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_pkey PRIMARY KEY (koderuangan, koders, kodebed);


--
-- Name: daftar_pesan daftar_pesan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_pkey PRIMARY KEY (idtransaksimakan, idtransaksi, id_pesanan);


--
-- Name: dokter dokter_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.dokter
    ADD CONSTRAINT dokter_pkey PRIMARY KEY (username, nostr);


--
-- Name: faskes faskes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.faskes
    ADD CONSTRAINT faskes_pkey PRIMARY KEY (kode);


--
-- Name: gedung gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.gedung
    ADD CONSTRAINT gedung_pkey PRIMARY KEY (kodegedung);


--
-- Name: gejala_pasien gejala_pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.gejala_pasien
    ADD CONSTRAINT gejala_pasien_pkey PRIMARY KEY (nik, namagejala);


--
-- Name: hotel hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.hotel
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (kode);


--
-- Name: hotel_room hotel_room_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.hotel_room
    ADD CONSTRAINT hotel_room_pkey PRIMARY KEY (kodehotel, koderoom);


--
-- Name: jadwal_dokter jadwal_dokter_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_pkey PRIMARY KEY (nostr, username, kode_faskes, shift, tanggal);


--
-- Name: jadwal jadwal_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.jadwal
    ADD CONSTRAINT jadwal_pkey PRIMARY KEY (kode_faskes, shift, tanggal);


--
-- Name: kamar_rumah kamar_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.kamar_rumah
    ADD CONSTRAINT kamar_rumah_pkey PRIMARY KEY (koderumah, kodekamar);


--
-- Name: komorbid_pasien komorbid_pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.komorbid_pasien
    ADD CONSTRAINT komorbid_pasien_pkey PRIMARY KEY (nik, namakomorbid);


--
-- Name: memeriksa memeriksa_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_pkey PRIMARY KEY (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl);


--
-- Name: paket_makan paket_makan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.paket_makan
    ADD CONSTRAINT paket_makan_pkey PRIMARY KEY (kodehotel, kodepaket);


--
-- Name: pasien pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.pasien
    ADD CONSTRAINT pasien_pkey PRIMARY KEY (nik);


--
-- Name: pengguna_publik pengguna_publik_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.pengguna_publik
    ADD CONSTRAINT pengguna_publik_pkey PRIMARY KEY (username);


--
-- Name: reservasi_gedung reservasi_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_hotel reservasi_hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_rs reservasi_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_rumah reservasi_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: ruangan_gedung ruangan_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.ruangan_gedung
    ADD CONSTRAINT ruangan_gedung_pkey PRIMARY KEY (kodegedung, koderuangan);


--
-- Name: ruangan_rs ruangan_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.ruangan_rs
    ADD CONSTRAINT ruangan_rs_pkey PRIMARY KEY (koders, koderuangan);


--
-- Name: rumah rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.rumah
    ADD CONSTRAINT rumah_pkey PRIMARY KEY (koderumah);


--
-- Name: rumah_sakit rumah_sakit_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.rumah_sakit
    ADD CONSTRAINT rumah_sakit_pkey PRIMARY KEY (kode_faskes);


--
-- Name: telepon_faskes telepon_faskes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.telepon_faskes
    ADD CONSTRAINT telepon_faskes_pkey PRIMARY KEY (kode_faskes, notelp);


--
-- Name: tempat_tidur_gedung tempat_tidur_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.tempat_tidur_gedung
    ADD CONSTRAINT tempat_tidur_gedung_pkey PRIMARY KEY (kodegedung, koderuangan, notempattidur);


--
-- Name: tes tes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.tes
    ADD CONSTRAINT tes_pkey PRIMARY KEY (nik_pasien, tanggaltes);


--
-- Name: transaksi_booking transaksi_booking_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_pkey PRIMARY KEY (idtransaksibooking);


--
-- Name: transaksi_hotel transaksi_hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_hotel
    ADD CONSTRAINT transaksi_hotel_pkey PRIMARY KEY (idtransaksi);


--
-- Name: transaksi_makan transaksi_makan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_makan
    ADD CONSTRAINT transaksi_makan_pkey PRIMARY KEY (idtransaksi, idtransaksimakan);


--
-- Name: transaksi_rs transaksi_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_rs
    ADD CONSTRAINT transaksi_rs_pkey PRIMARY KEY (idtransaksi);


--
-- Name: transaksi_rumah transaksi_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_rumah
    ADD CONSTRAINT transaksi_rumah_pkey PRIMARY KEY (idtransaksi);


--
-- Name: ventilator ventilator_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.ventilator
    ADD CONSTRAINT ventilator_pkey PRIMARY KEY (koders, kodeventilator);


--
-- Name: memeriksa batas_pemeriksaan; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER batas_pemeriksaan BEFORE INSERT OR UPDATE OF nik_pasien ON siruco.memeriksa FOR EACH ROW EXECUTE PROCEDURE siruco.check_jml_pasien();


--
-- Name: bed_rs bed_in; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER bed_in AFTER INSERT ON siruco.bed_rs FOR EACH ROW EXECUTE PROCEDURE siruco.bed_amount();


--
-- Name: akun_pengguna password_violation; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER password_violation BEFORE INSERT OR UPDATE OF username ON siruco.akun_pengguna FOR EACH ROW EXECUTE PROCEDURE siruco.check_password();


--
-- Name: reservasi_rs reservasi_in; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER reservasi_in AFTER INSERT ON siruco.reservasi_rs FOR EACH ROW EXECUTE PROCEDURE siruco.bed_amount();


--
-- Name: reservasi_hotel rsvhotel; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER rsvhotel AFTER INSERT ON siruco.reservasi_hotel FOR EACH ROW EXECUTE PROCEDURE siruco.rsvhotel();


--
-- Name: reservasi_rs trigger3; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER trigger3 AFTER INSERT ON siruco.reservasi_rs FOR EACH ROW EXECUTE PROCEDURE siruco.create_transaksi_rs();


--
-- Name: daftar_pesan trigger5; Type: TRIGGER; Schema: siruco; Owner: db202b04
--

CREATE TRIGGER trigger5 AFTER INSERT ON siruco.daftar_pesan FOR EACH ROW EXECUTE PROCEDURE siruco.total_bayar_makan();


--
-- Name: admin_satgas admin_satgas_idfaskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_idfaskes_fkey FOREIGN KEY (idfaskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin_satgas admin_satgas_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_username_fkey FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin admin_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.admin
    ADD CONSTRAINT admin_username_fkey FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bed_rs bed_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_koders_fkey FOREIGN KEY (koders, koderuangan) REFERENCES siruco.ruangan_rs(koders, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bed_rs bed_rs_koders_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_koders_fkey1 FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_pesan daftar_pesan_idtransaksimakan_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_idtransaksimakan_fkey FOREIGN KEY (idtransaksimakan, idtransaksi) REFERENCES siruco.transaksi_makan(idtransaksimakan, idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_pesan daftar_pesan_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_kodehotel_fkey FOREIGN KEY (kodehotel, kodepaket) REFERENCES siruco.paket_makan(kodehotel, kodepaket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dokter dokter_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.dokter
    ADD CONSTRAINT dokter_username_fkey FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: gejala_pasien gejala_pasien_nik_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.gejala_pasien
    ADD CONSTRAINT gejala_pasien_nik_fkey FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: hotel_room hotel_room_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.hotel_room
    ADD CONSTRAINT hotel_room_kodehotel_fkey FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal_dokter jadwal_dokter_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_kode_faskes_fkey FOREIGN KEY (kode_faskes, shift, tanggal) REFERENCES siruco.jadwal(kode_faskes, shift, tanggal) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal_dokter jadwal_dokter_nostr_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_nostr_fkey FOREIGN KEY (nostr, username) REFERENCES siruco.dokter(nostr, username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal jadwal_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.jadwal
    ADD CONSTRAINT jadwal_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kamar_rumah kamar_rumah_koderumah_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.kamar_rumah
    ADD CONSTRAINT kamar_rumah_koderumah_fkey FOREIGN KEY (koderumah) REFERENCES siruco.rumah(koderumah) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: komorbid_pasien komorbid_pasien_nik_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.komorbid_pasien
    ADD CONSTRAINT komorbid_pasien_nik_fkey FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: memeriksa memeriksa_nik_pasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_nik_pasien_fkey FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: memeriksa memeriksa_nostr_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_nostr_fkey FOREIGN KEY (nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl) REFERENCES siruco.jadwal_dokter(nostr, username, kode_faskes, shift, tanggal) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: paket_makan paket_makan_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.paket_makan
    ADD CONSTRAINT paket_makan_kodehotel_fkey FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pasien pasien_idpendaftar_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.pasien
    ADD CONSTRAINT pasien_idpendaftar_fkey FOREIGN KEY (idpendaftar) REFERENCES siruco.pengguna_publik(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengguna_publik pengguna_publik_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.pengguna_publik
    ADD CONSTRAINT pengguna_publik_username_fkey FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_gedung reservasi_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_kodegedung_fkey FOREIGN KEY (kodegedung, noruang, notempattidur) REFERENCES siruco.tempat_tidur_gedung(kodegedung, koderuangan, notempattidur) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_gedung reservasi_gedung_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_hotel reservasi_hotel_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_kodehotel_fkey FOREIGN KEY (kodehotel, koderoom) REFERENCES siruco.hotel_room(kodehotel, koderoom) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_hotel reservasi_hotel_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_koders_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey1 FOREIGN KEY (koders, koderuangan) REFERENCES siruco.ruangan_rs(koders, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_koders_fkey2; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey2 FOREIGN KEY (koders, kodeventilator) REFERENCES siruco.ventilator(koders, kodeventilator) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_koderuangan_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koderuangan_fkey FOREIGN KEY (koderuangan, koders, kodebed) REFERENCES siruco.bed_rs(koderuangan, koders, kodebed) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rumah reservasi_rumah_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rumah reservasi_rumah_koderumah_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_koderumah_fkey FOREIGN KEY (koderumah, kodekamar) REFERENCES siruco.kamar_rumah(koderumah, kodekamar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ruangan_gedung ruangan_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.ruangan_gedung
    ADD CONSTRAINT ruangan_gedung_kodegedung_fkey FOREIGN KEY (kodegedung) REFERENCES siruco.gedung(kodegedung) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ruangan_rs ruangan_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.ruangan_rs
    ADD CONSTRAINT ruangan_rs_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rumah_sakit rumah_sakit_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.rumah_sakit
    ADD CONSTRAINT rumah_sakit_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: telepon_faskes telepon_faskes_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.telepon_faskes
    ADD CONSTRAINT telepon_faskes_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tempat_tidur_gedung tempat_tidur_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.tempat_tidur_gedung
    ADD CONSTRAINT tempat_tidur_gedung_kodegedung_fkey FOREIGN KEY (kodegedung, koderuangan) REFERENCES siruco.ruangan_gedung(kodegedung, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tes tes_nik_pasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.tes
    ADD CONSTRAINT tes_nik_pasien_fkey FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_idtransaksibooking_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_idtransaksibooking_fkey FOREIGN KEY (idtransaksibooking) REFERENCES siruco.transaksi_hotel(idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_kodepasien_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_kodepasien_fkey1 FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_hotel(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_hotel transaksi_hotel_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_hotel
    ADD CONSTRAINT transaksi_hotel_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_makan transaksi_makan_idtransaksi_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_makan
    ADD CONSTRAINT transaksi_makan_idtransaksi_fkey FOREIGN KEY (idtransaksi) REFERENCES siruco.transaksi_hotel(idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_rs transaksi_rs_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_rs
    ADD CONSTRAINT transaksi_rs_kodepasien_fkey FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rs(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_rumah transaksi_rumah_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.transaksi_rumah
    ADD CONSTRAINT transaksi_rumah_kodepasien_fkey FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rumah(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ventilator ventilator_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b04
--

ALTER TABLE ONLY siruco.ventilator
    ADD CONSTRAINT ventilator_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

