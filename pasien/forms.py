from django import forms
from django.forms import Form
from django.db import connection, transaction, IntegrityError


class RegisterPasienForm(forms.Form):
    pendaftar = forms.CharField(label="Pendaftar", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}),
    max_length=50)

    nik = forms.CharField(label="NIK Pasien", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=20)
    
    nama_pasien = forms.CharField(label="Nama Pasien", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=50)
    
    no_hp = forms.CharField(label="Nomor handphone", required=True,
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=12)

    no_telp = forms.CharField(label="Nomor telepon", required=True,
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=20)
    
    KTP_Jalan = forms.CharField(label="Jalan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    KTP_Kelurahan = forms.CharField(label="Kelurahan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    KTP_Kecamatan= forms.CharField(label="Kecamatan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)
    
    KTP_KabKot = forms.CharField(label="Kabupaten / Kota", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    KTP_Prov = forms.CharField(label="Provinsi", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    Dom_Jalan = forms.CharField(label="Jalan", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    Dom_Kelurahan = forms.CharField(label="Kelurahan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    Dom_Kecamatan = forms.CharField(label="Kecamatan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    Dom_KabKot = forms.CharField(label="Kabupaten / Kota", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    Dom_Prov = forms.CharField(label="Provinsi", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

class UpdatePasien(forms.Form):
    idpendaftar = forms.CharField(label="Pendaftar", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}),
    max_length=50)

    nik = forms.CharField(label="NIK Pasien", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}),
    max_length=20)
    
    nama = forms.CharField(label="Nama Pasien", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}),
    max_length=50)
    
    nohp = forms.CharField(label="Nomor handphone", required=True,
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=12)

    notelp = forms.CharField(label="Nomor telepon", required=True,
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=20)
    
    ktp_jalan = forms.CharField(label="Jalan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    ktp_kelurahan = forms.CharField(label="Kelurahan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    ktp_kecamatan = forms.CharField(label="Kecamatan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)
    
    ktp_kabkot = forms.CharField(label="Kabupaten / Kota", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    ktp_prov = forms.CharField(label="Provinsi", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    dom_jalan = forms.CharField(label="Jalan", required=True, 
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    dom_kelurahan = forms.CharField(label="Kelurahan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    dom_kecamatan = forms.CharField(label="Kecamatan", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    dom_kabkot = forms.CharField(label="Kabupaten / Kota", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)

    dom_prov = forms.CharField(label="Provinsi", required=True,
    widget=forms.TextInput(attrs={'class': 'form-control'}),
    max_length=30)
