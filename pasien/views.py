from django.shortcuts import render, redirect, get_object_or_404
from django.db import connection, transaction, IntegrityError
from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required
from purple.views import cursor_fetchall
from django.urls import reverse

# Create your views here.
def create_pasien(request):
    if request.session.get("role") != "PENGGUNA_PUBLIK":
        print("masuk role")
        return redirect('pasien:create_pasien')

    if request.method == 'POST':
        pendaftar = request.POST['pendaftar']
        nik = request.POST['nik']
        nama_pasien = request.POST['nama_pasien']
        no_hp = request.POST['no_hp']
        no_telp = request.POST['no_telp']
        KTP_Jalan = request.POST['KTP_Jalan']
        KTP_Kelurahan = request.POST['KTP_Kelurahan']
        KTP_Kecamatan = request.POST['KTP_Kecamatan']
        KTP_KabKot = request.POST['KTP_KabKot']
        KTP_Prov = request.POST['KTP_Prov']
        Dom_Jalan = request.POST['Dom_Jalan']
        Dom_Kelurahan = request.POST['Dom_Kelurahan']
        Dom_Kecamatan = request.POST['Dom_Kecamatan']
        Dom_KabKot = request.POST['Dom_KabKot']
        Dom_Prov = request.POST['Dom_Prov']

        if pendaftar is None or nik is None or nama_pasien is None or no_hp is None or no_telp is None or KTP_Jalan is None or KTP_Jalan is None or KTP_Kelurahan is None or KTP_Kecamatan is None or KTP_KabKot is None or KTP_Prov is None or Dom_Jalan is None or Dom_Kelurahan is None or Dom_Kecamatan is None or Dom_KabKot is None or Dom_Prov is None:
            pendaftar = request.session.get("username")
            data = {'pendaftar': pendaftar}
            form = RegisterPasienForm(initial=data)
            return render(request, 'create_pasien.html', {'form': form, 'message': 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'})

        # if cek_pendaftar == pendaftar:
        #     pendaftar = request.session.get("username")
        #     data = {'pendaftar': pendaftar}
        #     form = RegisterPasienForm(initial=data)
        #     return render(request, 'create_pasien.html', {'form': form, 'message': 'Pasien hanya bisa didaftarkan oleh satu pengguna publik'})

        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute("INSERT INTO siruco.PASIEN VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                        [nik, pendaftar, nama_pasien, KTP_Jalan, KTP_Kelurahan, KTP_Kecamatan,
                        KTP_KabKot,	KTP_Prov, Dom_Jalan, Dom_Kelurahan,	Dom_Kecamatan,	
                        Dom_KabKot,	Dom_Prov, no_telp, no_hp])
        return redirect('pasien:list_pasien')

    elif request.method == 'GET':
        pendaftar = request.session.get("username")
        data = {'pendaftar': pendaftar}
        if pendaftar is None:
            print("masuk none yg atas")
            return redirect('pasien:create_pasien')

        form = RegisterPasienForm(initial=data)
    context = {'form': form}
    return render(request, "create_pasien.html", context)

@user_login_required
def list_pasien(request):
    pendaftar = request.session.get("username")
    with connection.cursor() as cursor:
        cursor.execute("SELECT nik, nama FROM SIRUCO.PASIEN where idpendaftar = %s", [pendaftar])
        data_pasien = cursor.fetchall()
    
    if len(data_pasien) == 0:
        return render(request, 'list_pasien.html', {'data': data_pasien, 'message': 'Anda belum pernah mendaftarkan pasien.'})
        
    context = {'data': data_pasien}
    return render(request, "list_pasien.html", context)

@user_login_required
def detail_pasien(request):
    if request.method == 'GET':
        nik = request.GET.get('nik') 
        
        if nik is None:
            return redirect('pasien:list_pasien')
            
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute("SELECT * FROM SIRUCO.PASIEN WHERE nik=%s", [nik])
            data_pasien = cursor.fetchone()
        print(data_pasien)
    context = {'data': data_pasien}
    return render(request, "detail_pasien.html", context)

@user_login_required
def update_pasien(request):
    if request.session.get("role") != "PENGGUNA_PUBLIK":
        return redirect('pasien:list_pasien')

    if request.method == 'POST':
        pendaftar = request.POST['idpendaftar']
        nik = request.POST['nik']
        nama_pasien = request.POST['nama']
        no_hp = request.POST['nohp']
        no_telp = request.POST['notelp']
        KTP_Jalan = request.POST['ktp_jalan']
        KTP_Kelurahan = request.POST['ktp_kelurahan']
        KTP_Kecamatan = request.POST['ktp_kecamatan']
        KTP_KabKot = request.POST['ktp_kabkot']
        KTP_Prov = request.POST['ktp_prov']
        Dom_Jalan = request.POST['dom_jalan']
        Dom_Kelurahan = request.POST['dom_kelurahan']
        Dom_Kecamatan = request.POST['dom_kecamatan']
        Dom_KabKot = request.POST['dom_kabkot']
        Dom_Prov = request.POST['dom_prov']

        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                '''
                UPDATE PASIEN 
                SET notelp = %s, nohp = %s,
                KTP_Jalan = %s, KTP_Kelurahan = %s, KTP_Kecamatan = %s, KTP_KabKot = %s, KTP_Prov = %s, 
                Dom_Jalan = %s, Dom_Kelurahan = %s, Dom_Kecamatan = %s, Dom_KabKot = %s, Dom_Prov = %s
                WHERE nik = %s and idpendaftar = %s;
                ''',
                [no_telp, no_hp, 
                KTP_Jalan, KTP_Kelurahan, KTP_Kecamatan, KTP_KabKot, KTP_Prov, 
                Dom_Jalan, Dom_Kelurahan, Dom_Kecamatan, Dom_KabKot, Dom_Prov,
                nik, pendaftar])
        return redirect('pasien:list_pasien')

    elif request.method == 'GET':
        pendaftar = request.session.get("username")
        nik_pasien = request.GET.get('nik') 
            
        if pendaftar is None and nik_pasien is None:
            print("masuk none yg atas")
            return redirect('pasien:list_pasien')
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.PASIEN where nik=%s LIMIT 1;", [nik_pasien])
            data = cursor.fetchone()
            if data is None:
                return redirect('pasien:list_pasien')
            columns = [col[0] for col in cursor.description]
            data_pasien = dict(zip(columns, data))
            print(data_pasien)

        form = UpdatePasien(initial=data_pasien)
    context = {'form': form}
    return render(request, "update_pasien.html", context)

@user_login_required
def delete_pasien(request):
    if request.method == 'GET':
        print("masuk delete")
        nik = request.GET.get('nik') 
        
        if nik is None:
            return redirect('pasien:list_pasien')
            
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute("DELETE FROM SIRUCO.PASIEN WHERE nik=%s", [nik])
        
    return HttpResponseRedirect(reverse('pasien:list_pasien'))