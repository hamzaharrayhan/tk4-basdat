from django.urls import path, include
from .views import *

app_name = 'pasien'

urlpatterns = [
    path('list_pasien/', list_pasien, name="list_pasien"),
    path('create_pasien/', create_pasien, name="create_pasien"),
    path('detail_pasien/', detail_pasien, name="detail_pasien"),
    path('update_pasien/', update_pasien, name="update_pasien"),
    path('delete_pasien/', delete_pasien, name="delete_pasien"),
]