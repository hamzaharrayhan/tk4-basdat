from django import forms
from django.forms import Form, ValidationError
from django.db import connection, transaction, IntegrityError
from django.http import request
from django.conf import settings

class CreateAppointment(forms.Form):
	pos_attrs = {
		'class': 'form-control',

	}
	email_dokter = forms.CharField(label="Email Dokter", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	kode_faskes = forms.CharField(label="Kode Faskes", required=True, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	tanggal = forms.CharField(label="Tanggal Praktek", required=True, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
	shift = forms.CharField(label="Shift Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))

class UpdateAppointment(forms.Form):
	nik_pasien = forms.CharField(label="NIK Pasien", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	email_dokter = forms.CharField(label="Email Dokter", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	kode_faskes = forms.CharField(label="Kode Faskes", required=False, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	tanggal = forms.CharField(label="Tanggal Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
	shift = forms.CharField(label="Shift Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	rekomendasi = forms.CharField(label="Rekomendasi", required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))

class CreateRuanganRS(forms.Form):
	# KODE_RS_CHOICES = []

	# def __init__(self, *args, **kwargs):
	# 	super().__init__(*args, **kwargs)
	# 	with connection.cursor() as cursor:
	# 		cursor.execute("SELECT Kode_Faskes, Kode_Faskes FROM SIRUCO.RUMAH_SAKIT")
	# 		self.fields['kode_rs'].choices = cursor.fetchall()

	# pos_attrs = {
	# 	'class': 'form-control',

	# }

	# kode_rs = forms.ChoiceField(label='Kode Rumah Sakit', required=True, choices= KODE_RS_CHOICES, widget=forms.Select(attrs={
	# 	'class': 'form-control'}))

	# kode_ruangan = forms.CharField(label="Kode Ruangan", required=False, widget=forms.TextInput(attrs={
	# 	'class': 'form-control',
	# 	'placeholder' : 'Rxx (generated automatically)',
	# 	'required' : True,
	# 	'readonly': True}))

	harga_ruangan = forms.CharField(label="Harga Ruangan", required=True, max_length=10, widget=forms.TextInput(attrs={
		'class': 'form-control'}))

	tipe = forms.CharField(label="Tipe", required=True, max_length=10, widget=forms.TextInput(attrs={
		'class': 'form-control'}))   


class CreateBedRS(forms.Form):
	KODE_RS_CHOICES = []

	kode_rs = forms.ChoiceField(label='Kode Rumah Sakit', required=True, choices= KODE_RS_CHOICES, widget=forms.Select(attrs={
		'class': 'form-control',
		'id' : 'kode_rs',
		'required' : True}))

	def __init__(self, *args, **kwargs):
		super(CreateBedRS, self).__init__(*args, **kwargs)
		with connection.cursor() as cursor:
			cursor.execute("SELECT Kode_Faskes, Kode_Faskes FROM SIRUCO.RUMAH_SAKIT")
			self.fields['kode_rs'].choices = cursor.fetchall()
			

class UpdateRuanganRS(forms.Form):
	koders = forms.CharField(label='Kode Rumah Sakit', required=False, widget=forms.TextInput(attrs={
		'class': 'form-control',
		'readonly': True}))

	koderuangan = forms.CharField(label="Kode Ruangan", required=False, widget=forms.TextInput(attrs={
		'class': 'form-control',
		'readonly': True}))

	harga = forms.CharField(label="Harga Ruangan", required=True, max_length=10, widget=forms.TextInput(attrs={
		'class': 'form-control'}))

	tipe = forms.CharField(label="Tipe", required=True, max_length=10, widget=forms.TextInput(attrs={
		'class': 'form-control'})) 

	def save(self):
		cd = self.cleaned_data
		print(cd)
		with connection.cursor() as cursor:
			cursor.execute("SET search_path to SIRUCO")
			cursor.execute("""
			UPDATE SIRUCO.RUANGAN_RS SET tipe = %s, harga = %s
			WHERE koders=%s AND koderuangan =%s """,
						   [cd.get("tipe"), cd.get("harga"),
							cd.get("koders"), cd.get("koderuangan")])
	




