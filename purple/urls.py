from django.urls import path, include
from .views import *

app_name = 'purple'
urlpatterns = [ 
	path('create_jadwal_dokter/', create_jadwal_dokter, name='create_jadwal_dokter'), 
	path('list_jadwal_dokter/', list_jadwal_dokter, name='list_jadwal_dokter'),
	path('list_jadwal/', list_jadwal, name='list_jadwal'),

	path('create_appointment/', create_appointment, name='create_appointment'), 
	path('list_appointment/', list_appointment, name='list_appointment'),
	path('form_appointment/', form_appointment, name='form_appointment'),
	path('delete_appointment/', delete_appointment, name='delete_appointment'),
	path('update_appointment/', update_appointment, name='update_appointment'),
	path('ajaxAppointment/', ajaxAppointment, name='ajaxAppointment'),

	path('create_ruangan_rs/', create_ruangan_rs, name='create_ruangan_rs'),
	path('list_ruangan_rs/', list_ruangan_rs, name='list_ruangan_rs'),
	path('update_ruangan_rs/', update_ruangan_rs, name='update_ruangan_rs'),
	path('ajaxRuanganRS/', ajaxRuanganRS, name='ajaxRuanganRS'),
	path('ajaxRuanganRS2/', ajaxRuanganRS2, name='ajaxRuanganRS2'),

	path('create_bed_rs/', create_bed_rs, name='create_bed_rs'), 
	path('list_bed_rs/', list_bed_rs, name='list_bed_rs'), 
	path('ajaxBedRS/', ajaxBedRS, name='ajaxBedRS'),
	path('ajaxBedRS2/', ajaxBedRS2, name='ajaxBedRS2'),
	path('delete_bed_rs/', delete_bed_rs, name='delete_bed_rs'),
]
