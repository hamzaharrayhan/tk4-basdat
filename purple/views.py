from django.shortcuts import render, redirect, get_object_or_404
from django.db import connection, transaction, IntegrityError
from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required
from django.urls import reverse

def cursor_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

#================================================ JADWAL DOKTER ==============================================================

# Create your views here.
def create_jadwal_dokter(request):
    if request.session.get("role") != "DOKTER":
	    return redirect('purple:create_jadwal_dokter')
    if request.method == 'GET':
        username = request.session.get('username')
        kode_faskes = request.GET.get('kode_faskes')
        shift = request.GET.get('shift')
        tanggal = request.GET.get('tanggal') 
        print(username, kode_faskes, shift, tanggal)

        if username is None or kode_faskes is None or shift is None or tanggal is None:
            print("masuk none yg atas")
            return redirect('purple:list_jadwal')

        with connection.cursor() as cursor:
            cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [username])
            no_str = cursor.fetchone()[0]
            if no_str is None:
                print("masuk none yg no str")
                return redirect('purple:list_jadwal')
            print(no_str)
            cursor.execute("""
            SELECT * FROM SIRUCO.JADWAL_DOKTER 
            where nostr=%s AND username=%s AND kode_faskes=%s 
            AND shift=%s AND tanggal=%s LIMIT 1;
            """, [no_str, username, kode_faskes, shift, tanggal])
            data_jadwal = cursor.fetchone()

            if data_jadwal is not None:
                return redirect('purple:list_jadwal')

            cursor.execute("INSERT INTO SIRUCO.JADWAL_DOKTER VALUES(%s,%s,%s,%s,%s,%s)", 
                            [no_str, username, kode_faskes, shift, tanggal, 0])
        return redirect('purple:list_jadwal_dokter') 
	
@user_login_required
def list_jadwal(request):
    username = request.session.get('username')
    print(username)
    with connection.cursor() as cursor:
        cursor.execute(
            """
            (select * from siruco.jadwal) 
            EXCEPT
            (select kode_faskes, shift, tanggal from siruco.jadwal_dokter j where username=%s)
            """,[username])
        data_jadwal = cursor_fetchall(cursor)
        print(data_jadwal)
    context = {'data': data_jadwal}
    return render(request, "CreateJadwalDokter.html", context)

@user_login_required
def list_jadwal_dokter(request):
    dokter = request.session.get("username")

    if request.session.get("role") == "DOKTER":
        with connection.cursor() as cursor:
            cursor.execute("SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.JADWAL_DOKTER WHERE Username = %s", [dokter])
            data_jadwal_dokter = cursor_fetchall(cursor)
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.JADWAL_DOKTER")
            data_jadwal_dokter = cursor_fetchall(cursor)
            
    context = {'data': data_jadwal_dokter}
    return render(request, "ListJadwalDokter.html", context)

#================================================ APPOINTMENT ==============================================================

@user_login_required
def list_appointment(request):
    siapa = request.session.get("username")
    if request.session.get("role") == "ADMIN_SATGAS":
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.MEMERIKSA")
            data_appointment = cursor_fetchall(cursor)
    elif request.session.get("role") == "PENGGUNA_PUBLIK":
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.MEMERIKSA M JOIN SIRUCO.PASIEN P ON M.NIK_Pasien = P.NIK WHERE P.IdPendaftar = %s", [siapa])
            data_appointment = cursor_fetchall(cursor)
    elif request.session.get("role") == "DOKTER":
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.MEMERIKSA WHERE username_dokter = %s", [siapa])
            data_appointment = cursor_fetchall(cursor)
        
    context = {'data': data_appointment}
    return render(request, "ListAppointment.html", context)

def ajaxAppointment(request):
    print("MASUK AJAX APPOINTMENTTT")
    pendaftar = request.session.get("username")
    role = request.session.get("role")

    if role == "PENGGUNA_PUBLIK":
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK, NIK FROM SIRUCO.PASIEN where IdPendaftar = %s", [pendaftar])
            data = cursor.fetchall()
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK, NIK FROM SIRUCO.PASIEN")
            data = cursor.fetchall()
    nik_pasien = []
    for i in data:
        nik_pasien.append(i[0])
    return JsonResponse({'id': nik_pasien})


@user_login_required
def create_appointment(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.JADWAL_DOKTER")
        data_appointment = cursor_fetchall(cursor)
    context = {'data': data_appointment}
    return render(request, "CreateAppointment.html", context)

@user_login_required
def form_appointment(request):
    if request.session.get("role") != "ADMIN_SATGAS" and request.session.get("role") != "PENGGUNA_PUBLIK":
        print(request.session.get("role"))
        print("masuk role")
        return redirect('purple:create_appointment')

    if request.method == 'POST':
        nik_pasien = request.POST['nik_pasien']
        username = request.POST['email_dokter']
        kode_rs = request.POST['kode_faskes']
        shift = request.POST['shift']
        tanggal = request.POST['tanggal']
        rekomendasi = '-'
        print(nik_pasien, username, kode_rs, shift, tanggal, rekomendasi)

        data = {'email_dokter': username, 
        'kode_faskes': kode_rs, 
        'tanggal': tanggal,
        'shift': shift}

        with connection.cursor() as cursor:
            cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [username])
            no_str = cursor.fetchone()[0]
            print(no_str)
            
        if no_str is None:
            print("masuk none yg no str")
            return redirect('purple:form_appointment')

        try:
            with connection.cursor() as cursor:
                cursor.execute("SET search_path to SIRUCO")
                cursor.execute(
                    'INSERT INTO SIRUCO.MEMERIKSA VALUES (%s, %s, %s, %s, %s, %s, %s)', 
                    [nik_pasien, no_str, username, kode_rs, shift, tanggal, rekomendasi])
            return redirect('purple:list_appointment')
        except:
            form = CreateAppointment(initial=data)
            return render(request, 'FormAppointment.html', {'form': form, 'message': 'Silahkan pilih shift dan tanggal lainnya, karena shift dan tanggal yang dipilih sudah penuh.'})

    elif request.method == 'GET':
        username = request.GET.get('username')
        kode_faskes = request.GET.get('kode_faskes')
        shift = request.GET.get('shift')
        tanggal = request.GET.get('tanggal') 
        print(username, kode_faskes, shift, tanggal)

        data = {'email_dokter': username, 
        'kode_faskes': kode_faskes, 
        'tanggal': tanggal,
        'shift': shift}

        print(data)

        if username is None or kode_faskes is None or shift is None or tanggal is None:
            print("masuk none yg atas")
            return redirect('purple:create_appointment')

        if data is None:
            return redirect('purple:create_appointment')

        form = CreateAppointment(initial=data)
    context = {'form': form}
    return render(request, "FormAppointment.html", context)

@user_login_required
def delete_appointment(request):
    if request.method == 'GET':
        nik_pasien = request.GET.get('nik_pasien')
        username = request.GET.get('username')
        kode_faskes = request.GET.get('kode_faskes')
        shift = request.GET.get('shift')
        tanggal = request.GET.get('tanggal') 
        print(username, kode_faskes, shift, tanggal)

        if nik_pasien is None and username is None and kode_faskes is None and shift is None and tanggal is None:
            print("masuk none yg atas")
            return redirect('purple:list_appointment')

        with connection.cursor() as cursor:
            cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [username])
            no_str = cursor.fetchone()[0]
            print(no_str)

        if no_str is None:
            print("masuk none yg no str")
            return redirect('purple:list_appointment')

        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                '''
                DELETE FROM SIRUCO.MEMERIKSA WHERE nik_pasien=%s 
                AND username_dokter=%s AND nostr=%s AND kode_faskes= %s 
                AND praktek_shift= %s AND praktek_tgl=%s
                ''', 
                [nik_pasien, username, no_str, kode_faskes, shift, tanggal])
    return HttpResponseRedirect(reverse('purple:list_appointment'))

@user_login_required
def update_appointment(request):
    if request.method == 'POST':
        nik_pasien = request.POST['nik_pasien']
        username = request.POST['email_dokter']
        kode_rs = request.POST['kode_faskes']
        shift = request.POST['shift']
        tanggal = request.POST['tanggal']
        rekomendasi = request.POST['rekomendasi']
        
        if rekomendasi is None:
            raise ValidationError("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu")

        with connection.cursor() as cursor:
            cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [username])
            no_str = cursor.fetchone()[0]
            print(no_str)

        if no_str is None:
            print("masuk none yg no str")
            return redirect('purple:update_appointment')

        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
			"""
			UPDATE SIRUCO.MEMERIKSA SET rekomendasi= %s
			WHERE nik_pasien=%s AND username_dokter=%s 
			AND nostr=%s AND kode_faskes= %s 
            AND praktek_shift= %s AND praktek_tgl=%s
			""",
			[rekomendasi, 
			nik_pasien, username, 
			no_str, kode_rs,
			shift, tanggal])
        return redirect('purple:list_appointment')

    elif request.method == 'GET':
        nik_pasien = request.GET.get('nik_pasien')
        username = request.GET.get('username')
        kode_faskes = request.GET.get('kode_faskes')
        shift = request.GET.get('shift')
        tanggal = request.GET.get('tanggal') 
        print(username, kode_faskes, shift, tanggal)

        data = {
        'nik_pasien': nik_pasien,
        'email_dokter': username, 
        'kode_faskes': kode_faskes, 
        'tanggal': tanggal,
        'shift': shift
        }

        if nik_pasien is None and username is None and kode_faskes is None and shift is None and tanggal is None:
            print("masuk none yg atas")
            return redirect('purple:list_appointment')

        with connection.cursor() as cursor:
            cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [username])
            no_str = cursor.fetchone()[0]
            print(no_str)

        if no_str is None:
            print("masuk none yg no str")
            return redirect('purple:list_appointment')
        
        form = UpdateAppointment(initial=data)
    context = {'form': form}
    return render(request, "UpdateAppointment.html", context)



#================================================ BED RS ==============================================================

@user_login_required
def list_bed_rs(request):
    kode_bed = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.BED_RS")
        data_bed_rs = cursor_fetchall(cursor)
        cursor.execute("SELECT DISTINCT kodebed FROM SIRUCO.RESERVASI_RS R WHERE tglkeluar > now() - interval '1 year'")
        bed_hapus = cursor_fetchall(cursor)
    
    for index in range(len(bed_hapus)):
        for key in bed_hapus[index]:
            kode_bed.append(bed_hapus[index][key])
    
    print(kode_bed)
    context = {'data': data_bed_rs, 'kode_bed': kode_bed}
    return render(request, "ListBedRS.html", context)

@user_login_required
def create_bed_rs(request):
    form = CreateBedRS()
    if request.method == 'POST':
        kode_rs = request.POST['kode_rs']
        kode_ruangan = request.POST['ruanganRS']
        kode_bed = request.POST['bedRS']

        data_baru = [kode_ruangan, kode_rs, kode_bed]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                'INSERT INTO SIRUCO.BED_RS VALUES (%s, %s, %s)', data_baru)
        return redirect('purple:list_bed_rs')
    return render(request, 'CreateBedRS.html', {'form': form})

def ajaxBedRS(request):
    print("masuk ajax")
    print(request.GET.get('kode_faskes'))
    kode_faskes = request.GET.get('kode_faskes')
    print(kode_faskes)
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT koderuangan FROM SIRUCO.RUANGAN_RS WHERE KODERS = %s", [kode_faskes])
        data = cursor.fetchall()
        print(data)
    list_ruangan = []
    for i in data:
        list_ruangan.append(i[0])

    return JsonResponse({'id': list_ruangan})

def ajaxBedRS2(request):
    print("masuk ajax 2222")
    kode_ruangan = request.GET.get('kode_ruangan')
    print(request.GET.get('kode_ruangan'))
    kode_bed_new = ''
    with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                "SELECT kodebed FROM SIRUCO.BED_RS WHERE koderuangan = %s ORDER BY kodebed DESC LIMIT 1", [kode_ruangan])
            try:
                old = cursor.fetchone()[0]
                kode_bed_new = old[0] + str(int(old[1]) + 1) + old[2:]
            except:
                cursor.execute(
                "SELECT koders FROM SIRUCO.RUANGAN_RS WHERE koderuangan = %s LIMIT 1", [kode_ruangan])
                kode_faskes = cursor.fetchone()[0]
                kode_bed_new = "B1" + kode_faskes
    return JsonResponse({'id': kode_bed_new})

@user_login_required
def delete_bed_rs(request):
    if request.method == 'GET':
        kode_bed = []
        if request.GET.get('koders') is not None and request.GET.get('koderuangan') is not None and request.GET.get('kodebed') is not None:
            koders = request.GET.get('koders')
            koderuangan = request.GET.get('koderuangan')
            kodebed = request.GET.get('kodebed')
            print(koders, koderuangan, kodebed)

            with connection.cursor() as cursor:
                cursor.execute("SELECT DISTINCT kodebed FROM SIRUCO.RESERVASI_RS R WHERE tglkeluar > now() - interval '1 year'")
                bed_hapus = cursor_fetchall(cursor)
        
            for index in range(len(bed_hapus)):
                for key in bed_hapus[index]:
                    kode_bed.append(bed_hapus[index][key])

            if kodebed not in kode_bed:
                with connection.cursor() as cursor:
                    cursor.execute("SET search_path to SIRUCO")
                    cursor.execute(
                        'DELETE FROM SIRUCO.BED_RS WHERE koders = %s AND koderuangan = %s AND kodebed = %s', [koders, koderuangan, kodebed])
            else:
                with connection.cursor() as cursor:
                    cursor.execute("SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.BED_RS")
                    data_bed_rs = cursor_fetchall(cursor)
                context = {'data': data_bed_rs, 'message': 'Bed yang ingin anda hapus sedang direservasi oleh pasien.'}
                return render(request, "ListBedRS.html", context)
    return HttpResponseRedirect(reverse('purple:list_bed_rs'))

#================================================ RUANGAN RS ==============================================================

@user_login_required
def create_ruangan_rs(request):
    form = CreateRuanganRS()
    if request.method == 'POST':
        kode_rs = request.POST['kodeRS']
        kode_ruangan = request.POST['ruanganRS']
        jml_bed = 0
        harga_ruangan = request.POST['harga_ruangan']
        tipe = request.POST['tipe']

        data_baru = [kode_rs, kode_ruangan, tipe, jml_bed, harga_ruangan]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                'INSERT INTO SIRUCO.RUANGAN_RS VALUES (%s, %s, %s, %s, %s)', data_baru)
        return redirect('purple:list_ruangan_rs')
    return render(request, 'CreateRuanganRS.html', {'form': form})
	
@user_login_required
def list_ruangan_rs(request):
	with connection.cursor() as cursor:
		cursor.execute("SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.RUANGAN_RS")
		data_ruangan_rs = cursor_fetchall(cursor)
	context = {'data': data_ruangan_rs}
	return render(request, "ListRuanganRS.html", context)

@user_login_required
def update_ruangan_rs(request):
    if request.session.get("role") != "ADMIN_SATGAS":
	    return redirect('purple:list_ruangan_rs')
    if request.method == 'POST':
        form = UpdateRuanganRS(request.POST)
        if form.is_valid():
            form.save()
            return redirect("purple:list_ruangan_rs")
    elif request.method == 'GET':
        if request.GET.get('koders') is None and request.GET.get('koderuangan') is None:
            return redirect('purple:list_ruangan_rs')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RUANGAN_RS where koders=%s AND koderuangan=%s LIMIT 1;",
                            [request.GET.get('koders'), request.GET.get('koderuangan')])
            data = cursor.fetchone()
            print(data)
            if data is None:
                return redirect('purple:list_ruangan_rs')
            columns = [col[0] for col in cursor.description]
            data_ruangan_rs = dict(zip(columns, data))
            print(data_ruangan_rs)
        form = UpdateRuanganRS(initial=data_ruangan_rs)
    context = {'form': form}
    return render(request, "UpdateRuanganRS.html", context)

def ajaxRuanganRS(request):
    print("masuk ajax ruangan rs")
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_faskes FROM SIRUCO.RUMAH_SAKIT")
        data = cursor.fetchall()
        print(data)
    kode_rs = []
    for i in data:
        kode_rs.append(i[0])
    return JsonResponse({'id': kode_rs})

def ajaxRuanganRS2(request):
    print("masuk ajax rs 2222")
    kode_rs = request.GET.get('kode_rs')
    kode_ruangan_new = ''
    with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                "SELECT koderuangan FROM SIRUCO.RUANGAN_RS WHERE koders = %s ORDER BY koderuangan DESC LIMIT 1", [kode_rs])
            try:
                old = cursor.fetchone()[0]
                kode_ruangan_new = old[0] + str(int(old[1]) + 1) + old[2:]
            except:
                kode_ruangan_new = "R1" + kode_rs
    return JsonResponse({'id': kode_ruangan_new})