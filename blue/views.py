from django.db import connection
from django.shortcuts import render, redirect
from blue.forms import *
from django.contrib import messages
from datetime import datetime

# Create your views here.

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        username = request.session.get('username')
        if username is None:
            return redirect('/login/')
        else:
            return function(request, *args, **kwargs)
    return wrapper

def cursor_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@user_login_required
def reservasirs(request):
    
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/list-reservasi-rs/')
    if request.method == 'POST':
        form = CreateReservasi(request.POST)
        sem = request.POST.get('KodeRuangan')
        form.fields['KodeRuangan'].choices = [(sem,sem)]
        sem2 = request.POST.get('KodeBed')
        form.fields['KodeBed'].choices = [(sem2,sem2)]
        if form.is_valid():
            try:
                form.save()
                return redirect('/list-reservasi-rs/')
            except:
                messages.warning(request, 'Pasien sudah terdaftar')
    if request.method == 'GET':
        form = CreateReservasi()
    context = {'form': form}
    return render(request, "reservasi_rumah_sakit.html", context)

@user_login_required
def listreservasirs (request):
    if request.session.get("role") == 'ADMIN_SATGAS':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS")
            data_reservasi_rs = cursor_fetchall(cursor)
        context = {'data': data_reservasi_rs,'today':datetime.now().date()}
        
    else:
        username = request.session.get('username')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS RES, SIRUCO.PASIEN P WHERE RES.kodepasien = P.NIK AND P.IdPendaftar = %s",[username])
            data_reservasi_rs = cursor_fetchall(cursor)
        context = {'data': data_reservasi_rs}
    
    return render(request, "ListReservasiRS.html", context)

def load_semuaruangan(request):
    KodeRS = request.GET.get('KodeRS')
    with connection.cursor() as cursor:
            cursor.execute("SELECT KodeRuangan FROM SIRUCO.RUANGAN_RS RRS WHERE  RRS.KodeRS = %s",[KodeRS])
            SemuaRuangan = cursor_fetchall(cursor)
            print(SemuaRuangan)
   
    return render(request, 'hr/ruangan_dropdown_list_options.html', {'SemuaRuangan':SemuaRuangan })

def load_semuabed(request):
    KodeRuangan = request.GET.get('KodeRuangan')
    print(KodeRuangan)
    with connection.cursor() as cursor:
            cursor.execute("SELECT KodeBed FROM SIRUCO.BED_RS BRS WHERE  BRS.KodeRuangan = %s",[KodeRuangan])
            SemuaBed = cursor_fetchall(cursor)
            print(SemuaBed)
    return render(request, 'hr/bed_dropdown_list_options.html', {'SemuaBed':SemuaBed })

@user_login_required
def delete_reservasi_rs(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/list-reservasi-rs/')
    if request.method == 'GET':
        pasien = request.GET.get('pasien')
        tgl =  request.GET.get('tgl').replace(".","")
        if None in [pasien,tgl]:
            return redirect('/list-reservasi-rs/')
        else:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.RESERVASI_RS WHERE KodePasien=%s AND TglMasuk=%s", [pasien,tgl])
            return redirect('/list-reservasi-rs/')

@user_login_required
def update_reservasi_rs(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/list-reservasi-rs/')
    if request.method == 'POST':
        form = UpdateReservasi(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/list-reservasi-rs/')
    elif request.method == 'GET':
        pasien = request.GET.get('pasien')
        tgl =  request.GET.get('tgl').replace(".","")
        if pasien is None or tgl is None:
            return redirect('/list-reservasi-rs/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS WHERE kodepasien=%s AND TglMasuk=%s",
                           [pasien, tgl])
            columns = [col[0] for col in cursor.description]
            data_reservasi = dict(zip(columns, cursor.fetchone()))
        form = UpdateReservasi(initial=data_reservasi)
    context = {'form': form}
    return render(request, "update_reservasi_rs.html", context)

@user_login_required
def faskes(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = CreateFaskes(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/list-faskes/')
            except:
                messages.warning(request, 'Kesalahan dalam memasukkan data')
    elif request.method == 'GET':
        cursor = connection.cursor()
        kodeFK = 'fk1'
        try:
            cursor.execute("SELECT kode FROM siruco.faskes ORDER BY kode desc limit 1")
            kodefkbefore = cursor.fetchall()[0][0][2:]
            kodeFK = 'fk' + str((int(kodefkbefore)+1))
        except:
            kodeFK = 'fk1'
        IndisialForm = {'Kode':kodeFK}
        form = CreateFaskes(initial=IndisialForm)
    context = {'form': form}
    return render(request, "buat_faskes.html", context)


##################################

@user_login_required
def listfaskes (request):
    if request.session.get("role") == 'ADMIN_SATGAS':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.FASKES")
            data_faskes = cursor_fetchall(cursor)
        context = {'data': data_faskes}
    else:
        return redirect('/')
    return render(request, "ListFaskes.html", context)

@user_login_required
def delete_faskes(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'GET':
        faskes = request.GET.get('faskes')
        if None in [faskes]:
            return redirect('/list-faskes/')
        else:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.FASKES WHERE kode=%s", [faskes])
            return redirect('/list-faskes/')

@user_login_required
def update_faskes(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = UpdateFaskes(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/list-faskes/')
        else:
            print('gagal')
    elif request.method == 'GET':
        faskes = request.GET.get('faskes')
        if faskes is None:
            return redirect('/list-faskes/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.FASKES WHERE kode=%s",
                           [faskes])
            columns = [col[0] for col in cursor.description]
            data_faskes = dict(zip(columns, cursor.fetchone()))
            print(data_faskes)
        form = UpdateFaskes(initial=data_faskes)
    context = {'form': form}
    return render(request, "update_faskes.html", context)

@user_login_required
def detail_faskes (request):
    if request.session.get("role") == 'ADMIN_SATGAS':
        faskes = request.GET.get('faskes')
        if faskes is None:
            return redirect('/list-faskes/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.FASKES WHERE kode=%s",[faskes])
            data_faskes = cursor_fetchall(cursor)
        context = {'data': data_faskes}
    else:
        return redirect('/')
    return render(request, "detail_faskes.html", context)

#####################################################

@user_login_required
def jadwal(request):
    
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = CreateJadwalFaskes(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/list-jadwal/')
            except:
                messages.warning(request, 'Jadwal sudah ada')
    if request.method == 'GET':
        form = CreateJadwalFaskes()
    context = {'form': form}
    return render(request, "buat_jadwal.html", context)

@user_login_required
def listjadwal (request):
    print(request.session.get("role"))
    if request.session.get("role") == 'ADMIN_SATGAS':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.JADWAL")
            data_jadwal = cursor_fetchall(cursor)
        context = {'data': data_jadwal}
    else:
        redirect('/')
    return render(request, "ListJadwal.html", context)

#################################################################

@user_login_required
def rumahsakit(request):
    
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = CreateRumahSakit(request.POST)
        sem = request.POST.get("kode_faskes")
        form.fields["kode_faskes"].choices = [(sem,sem)]
        if form.is_valid():
            try:
                form.save()
                return redirect('/list-rumah-sakit/') 
            except:
                messages.warning(request, 'Rumah Sakit Sudah ada')
    if request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT kode FROM siruco.faskes ORDER BY kode")
            PilihanFaskes = [(row[0],row[0]) for row in  cursor.fetchall()]
        form = CreateJadwalFaskes()
        form.fields["kode_faskes"].choices = PilihanFaskes
    context = {'form': form}
    return render(request, "buat_rumahsakit.html", context)

@user_login_required
def listrumahsakit (request):
    if request.session.get("role") == 'ADMIN_SATGAS':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RUMAH_SAKIT")
            data_rs = cursor_fetchall(cursor)
        context = {'data': data_rs}
    else:
        redirect('/')
    return render(request, "ListRumahSakit.html", context)

@user_login_required
def update_rumahsakit(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = UpdateRumahSakit(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/list-rumah-sakit/')
    elif request.method == 'GET':
        faskes = request.GET.get('faskes')
        if faskes is None:
            return redirect('/list-rumah-sakit/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RUMAH_SAKIT WHERE kode_faskes=%s",
                           [faskes])
            columns = [col[0] for col in cursor.description]
            data_faskes = dict(zip(columns, cursor.fetchone()))
            print(data_faskes)
        form = UpdateRumahSakit(initial=data_faskes)
    context = {'form': form}
    return render(request, "update_rumahsakit.html", context)


########################################################

@user_login_required
def listtransaksirs (request):
    if request.session.get("role") == 'ADMIN_SATGAS':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_RS")
            data_transaksi_rs = cursor_fetchall(cursor)
        context = {'data': data_transaksi_rs}
    else:
        redirect('/')
    return render(request, "ListTransaksiRS.html", context)


@user_login_required
def delete_transaksi_rs(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'GET':
        idtransaksi = request.GET.get('idtransaksi')
        if None in [idtransaksi]:
            return redirect('/list-transaksi-rs/')
        else:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.TRANSAKSI_RS WHERE idtransaksi=%s", [idtransaksi])
            return redirect('/list-transaksi-rs/')

@user_login_required
def update_transaksi_rs(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/')
    if request.method == 'POST':
        form = UpdateTransaksi(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/list-transaksi-rs/')
    elif request.method == 'GET':
        idtransaksi = request.GET.get('idtransaksi')
        if idtransaksi is None:
            return redirect('/list-transaksi-rs/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_RS WHERE idtransaksi=%s",
                           [idtransaksi])
            columns = [col[0] for col in cursor.description]
            data_transaksi = dict(zip(columns, cursor.fetchone()))
        form = UpdateTransaksi(initial=data_transaksi)
    context = {'form': form}
    return render(request, "update_transaksi_rs.html", context)
