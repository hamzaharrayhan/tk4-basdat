from django.urls import path
from . import views

app_name = 'blue'

urlpatterns = [
    path('buat-reservasi-rs/', views.reservasirs, name='reservasirs'),
    path('list-reservasi-rs/', views.listreservasirs, name='listreservasirs'),
    path('ajax/load-semuaruangan/', views.load_semuaruangan, name='ajax_load_semuaruangan'),
    path('ajax/load-semuabed/', views.load_semuabed, name='ajax_load_semuabed'),
    path('delete-reservasi-rs/',views.delete_reservasi_rs, name='delete-reservasi-rs'),
    path('update-reservasi-rs/',views.update_reservasi_rs, name='update-reservasi-rs'),
    path('buat-faskes/',views.faskes, name='buat-faskes'),
    path('list-faskes/', views.listfaskes, name='listfaskes'),
    path('delete-faskes/',views.delete_faskes, name='delete-faskes'),
    path('update-faskes/',views.update_faskes, name='update-faskes'),
    path('detail-faskes/',views.detail_faskes, name='detail-faskes'),
    path('buat-jadwal/',views.jadwal, name='buat-jadwal'),
    path('list-jadwal/', views.listjadwal, name='listjadwal'),
    path('buat-rumah-sakit/',views.rumahsakit, name='buat-rumah-sakit'),
    path('list-rumah-sakit/', views.listrumahsakit, name='listrumahsakit'),
    path('update-rumah-sakit/',views.update_rumahsakit, name='update-rumah-sakit'),
    path('list-transaksi-rs/', views.listtransaksirs, name='listtransaksirs'),
    path('delete-transaksi-rs/',views.delete_transaksi_rs, name='delete-transaksi-rs'),
    path('update-transaksi-rs/',views.update_transaksi_rs, name='update-transaksi-rs'),
]