from django import forms
from django.db import connection
from datetime import datetime
from django.core.exceptions import ValidationError

class CreateReservasi (forms.Form):
    cursor = connection.cursor()
    #NIK
    NIK = forms.ChoiceField(choices=[],
        widget=forms.Select(attrs={'class': 'form-control'}))

    #TanggalMasuk            
    TanggalMasuk = forms.DateField(
        initial=datetime.now())

    #Tanggal Keluar
    TanggalKeluar = forms.DateField(
        initial=datetime.now())

    def clean_TanggalKeluar (self):
        cleaned_data = super().clean()
        DateOut = cleaned_data['TanggalKeluar']
        DateIn = cleaned_data['TanggalMasuk']
        if DateIn > DateOut or DateIn == DateOut:
            raise forms.ValidationError("Tanggal masuk harus sebelum tanggal keluar")
        return DateOut

    #kode RS
    KodeRS = forms.ChoiceField(choices=[],
        widget=forms.Select(attrs={'class': 'form-control'}))
    def __init__(self, *args, **kwargs):
        super(CreateReservasi, self).__init__(*args, **kwargs)
        cursor = connection.cursor()
        cursor.execute("SELECT Kode_Faskes FROM siruco.RUMAH_SAKIT")
        PilihanKodeRS = [('------','------')]+[(row[0],row[0]) for row in  cursor.fetchall()]
        self.fields['KodeRS'].choices = PilihanKodeRS
        cursor.execute("SELECT NIK FROM siruco.PASIEN")
        PilihanNikPasien = [(row[0],row[0]) for row in  cursor.fetchall()]
        self.fields['NIK'].choices = PilihanNikPasien
    
    #Kode Ruangan
    kodeRSbersih = ""
    def clean_kodeRS (self):
        KodeRSbersih = self.cleaned_data.get("kodeRS")
    cursor.execute("SELECT KodeRuangan FROM siruco.Ruangan_RS WHERE kodeRS =%s",[kodeRSbersih])
    PilihanKodeRuangan = [(row[0],row[0]) for row in  cursor.fetchall()]
    KodeRuangan = forms.ChoiceField(choices=PilihanKodeRuangan, widget=forms.Select(attrs={'class': 'form-control'}))
    
    #KodeBed
    kodeRuanganBersih= ""
    def clean_kodeRuangan (self):
        KodeRuanganBersih = self.cleaned_data.get("kodeRuangan")
    cursor.execute("SELECT KodeBed FROM siruco.BED_RS WHERE kodeRuangan =%s",[kodeRuanganBersih])
    PilihanKodeBed = [(row[0],row[0]) for row in  cursor.fetchall()]
    KodeBed = forms.ChoiceField(choices=PilihanKodeBed,
    widget=forms.Select(attrs={'class': 'form-control'}))

    def save(self):
        NIK = self.cleaned_data.get('NIK')
        TanggalMasuk = self.cleaned_data.get('TanggalMasuk')
        TanggalKeluar = self.cleaned_data.get('TanggalKeluar')
        KodeRS = self.cleaned_data.get('KodeRS')
        KodeRuangan = self.cleaned_data.get('KodeRuangan')
        KodeBed = self.cleaned_data.get('KodeBed')
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO SIRUCO")
            cursor.execute("INSERT INTO siruco.RESERVASI_RS VALUES(%s,%s,%s,%s,%s,%s)",
                           [NIK, TanggalMasuk, TanggalKeluar,KodeRS,KodeRuangan,KodeBed])

class UpdateReservasi (forms.Form):
    cursor = connection.cursor()
    #NIK
    kodepasien = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))

    #TanggalMasuk            
    tglmasuk = forms.DateField(widget=forms.DateInput(attrs={'readonly':'readonly'}))

    #Tanggal Keluar
    tglkeluar = forms.DateField(
        initial=datetime.now())

    def clean_tglkeluar (self):
        cleaned_data = super().clean()
        DateOut = cleaned_data['tglkeluar']
        DateIn = cleaned_data['tglmasuk']
        if DateIn > DateOut or DateIn == DateOut :
            raise forms.ValidationError("Tanggal masuk harus sebelum tanggal keluar")
        return DateOut

    #kode RS
    koders = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    
    #Kode Ruangan
    koderuangan = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))

    #KodeBed
    kodebed = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))

    def save(self):
        kodepasien = self.cleaned_data.get('kodepasien')
        tglmasuk = self.cleaned_data.get('tglmasuk')
        tglkeluar = self.cleaned_data.get('tglkeluar')
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO SIRUCO")
            cursor.execute("UPDATE SIRUCO.Ruangan_rs SET jmlbed = 10")
            cursor.execute("UPDATE SIRUCO.RESERVASI_RS SET tglkeluar = %s where kodepasien = %s and tglmasuk = %s ",
                           [tglkeluar,kodepasien, tglmasuk])
        
class CreateFaskes (forms.Form):
    Kode = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    Tipe = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),max_length=30)
    Nama = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=50)
    StatusMilik = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),max_length=30)
    Jalan = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    Kelurahan = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    Kecamatan = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    Kabkot = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    Prov = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)

    def save(self):
        Kode = self.cleaned_data.get('Kode')
        Tipe = self.cleaned_data.get('Tipe')
        Nama = self.cleaned_data.get('Nama')
        StatusMilik = self.cleaned_data.get('StatusMilik')
        Jalan = self.cleaned_data.get('Jalan')
        Kelurahan = self.cleaned_data.get('Kelurahan')
        Kecamatan = self.cleaned_data.get('Kecamatan')
        Kabkot = self.cleaned_data.get('Kabkot')
        Prov = self.cleaned_data.get('Prov')
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO SIRUCO.FASKES VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                           [Kode,Tipe,Nama,StatusMilik,Jalan,Kelurahan,Kecamatan,Kabkot,Prov])

class UpdateFaskes (forms.Form):
    cursor = connection.cursor()

    kode = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    tipe = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),max_length=30)
    nama = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=50)
    statusmilik = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),max_length=30)
    jalan = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    kelurahan = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    kecamatan = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    kabkot = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    prov = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)

    def save(self):
        kode = self.cleaned_data.get('kode')
        tipe = self.cleaned_data.get('tipe')
        nama = self.cleaned_data.get('nama')
        statusmilik = self.cleaned_data.get('statusmilik')
        jalan = self.cleaned_data.get('jalan')
        kelurahan = self.cleaned_data.get('kelurahan')
        kecamatan = self.cleaned_data.get('kecamatan')
        kabkot = self.cleaned_data.get('kabkot')
        prov = self.cleaned_data.get('prov')
        with connection.cursor() as cursor:
            cursor.execute("UPDATE SIRUCO.FASKES SET Tipe = %s,Nama = %s,StatusMilik = %s,Jalan = %s,Kelurahan = %s,Kecamatan = %s,Kabkot = %s,Prov = %s WHERE Kode = %s ",
                           [tipe,nama,statusmilik,jalan,kelurahan,kecamatan,kabkot,prov,kode])

##################################

class CreateJadwalFaskes (forms.Form):
    
    #Kode Faskes
    kode_faskes = forms.ChoiceField(choices =[], widget=forms.Select(attrs={'class': 'form-control'}))
    def __init__(self, *args, **kwargs):
        cursor = connection.cursor()
        super(CreateJadwalFaskes, self).__init__(*args, **kwargs)
        cursor.execute("SELECT kode FROM siruco.faskes ORDER BY kode")
        PilihanFaskes = [(row[0],row[0]) for row in  cursor.fetchall()]
        self.fields['kode_faskes'].choices = PilihanFaskes
    shift = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),max_length=15)
    tanggal = forms.DateField(initial=datetime.now())


    def save(self):
        kode_faskes = self.cleaned_data.get('kode_faskes')
        shift = self.cleaned_data.get('shift')
        tanggal = self.cleaned_data.get('tanggal')
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO SIRUCO.JADWAL VALUES(%s,%s,%s)",
                           [kode_faskes,shift,tanggal])

###############################################

class CreateRumahSakit (forms.Form):
    cursor = connection.cursor()
    
    #Kode Faskes
    kode_faskes = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}))
    isrujukan = forms.BooleanField(required=False)


    def save(self):
        kode_faskes = self.cleaned_data.get('kode_faskes')
        isrujukan = self.cleaned_data.get('isrujukan')
        rujukan = 0
        if(isrujukan == True):
            rujukan=1
        print(rujukan)
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO SIRUCO.RUMAH_SAKIT VALUES(%s,%s)",[kode_faskes,rujukan])

class UpdateRumahSakit (forms.Form):
    cursor = connection.cursor()

    kode_faskes = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    isrujukan = forms.BooleanField(required=False)


    def save(self):
        kode_faskes = self.cleaned_data.get('kode_faskes')
        isrujukan = self.cleaned_data.get('isrujukan')
        rujukan = 0
        if(isrujukan == True):
            rujukan=1
        with connection.cursor() as cursor:
            cursor.execute("UPDATE SIRUCO.RUMAH_SAKIT SET isrujukan = %s WHERE kode_faskes=%s",[rujukan, kode_faskes])

class UpdateTransaksi (forms.Form):
    cursor = connection.cursor()
    idtransaksi = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    kodepasien = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    tanggalpembayaran = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}), required=False)
    waktupembayaran = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}), required=False)
    tglmasuk = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}), required=False)
    totalbiaya = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    statusbayar = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=15)


    def save(self):
        idtransaksi = self.cleaned_data.get('idtransaksi')
        statusbayar = self.cleaned_data.get('statusbayar')
        with connection.cursor() as cursor:
            cursor.execute("UPDATE SIRUCO.TRANSAKSI_RS SET statusbayar = %s WHERE idtransaksi=%s",[statusbayar, idtransaksi])