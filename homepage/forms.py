from django import forms
from django.db import connection

class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control'}),
        max_length=50)
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        max_length=20)

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT password FROM siruco.AKUN_PENGGUNA WHERE username=%s LIMIT 1;",
                [username])
            user_password = cursor.fetchone()[0]
        print(password)
        print(user_password)
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT * FROM siruco.AKUN_PENGGUNA WHERE username=%s LIMIT 1;",
                [username])
            data = cursor.fetchone()
        if data is None:
            self.add_error('username', 'username tidak terdaftar')
        elif user_password != password:
            self.add_error('password', 'Password salah')


class RolePickerForm(forms.Form):
    ROLE_CHOICE = [('PENGGUNA_PUBLIK', 'Pengguna Publik'),
                   ('ADMIN_SATGAS', 'Admin Satgas'),
                   ('DOKTER', 'Dokter'),
                   ('ADMIN', 'Admin')]
    peran = forms.ChoiceField(choices=ROLE_CHOICE,
                              widget=forms.Select(attrs={'class': 'form-control'}))

class RegisterUserForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control'}),
        max_length=50)

    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        max_length=20)

    def clean_username(self):
        data = self.cleaned_data.get('username')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM siruco.AKUN_PENGGUNA WHERE username=%s LIMIT 1",
                           [data])
            row = cursor.fetchone()
        if row is not None:
            raise forms.ValidationError("username sudah terdaftar")
        return data

    def save(self):
        username = self.cleaned_data.get('username')
        print("test12333")
        password = self.cleaned_data.get('password')
        print(self.cleaned_data)
     

class RegisterAdminSatgasForm(RegisterUserForm):
    KODE_FASKES_CHOICES = []

    kode_faskes = forms.ChoiceField(label='Kode Faskes', required=True, choices= KODE_FASKES_CHOICES, widget=forms.Select(attrs={
        'class': 'form-control',
		'id' : 'kode_faskes',
		'required' : True}))

    # with connection.cursor() as cursor:
    #     cursor.execute("SELECT Kode FROM SIRUCO.faskes")
    #     kode_faskes = cursor.fetchall()

    # id_faskes = forms.ChoiceField(
    #             choices=kode_faskes,
    #             widget=forms.Select(attrs={'class': 'form-control'})
    # )
    def __init__(self, *args, **kwargs):
        super(RegisterAdminSatgasForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT Kode, Kode FROM SIRUCO.FASKES")
            self.fields['kode_faskes'].choices = cursor.fetchall()

    def save(self):
        super().save()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        id_faskes = self.cleaned_data.get('kode_faskes')
        nama = "Admin"
        peran_pengguna = 'Admin Satgas'

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO siruco.AKUN_PENGGUNA VALUES(%s,%s,%s)",
                           [username, password, peran_pengguna])
            cursor.execute("INSERT INTO siruco.ADMIN VALUES(%s)", [username])
            cursor.execute("INSERT INTO siruco.ADMIN_SATGAS VALUES(%s,%s)", [username,id_faskes])
            
class RegisterDokterForm(RegisterUserForm):
    nomor_str = forms.CharField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
        max_length=20
    )
    nama = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    no_hp = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=12
    )
    gelar_depan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=10
    )
    gelar_belakang = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=10
    )

    def clean_nomor_str(self):
        data = self.cleaned_data.get('nomor_str')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM siruco.DOKTER WHERE nostr=%s LIMIT 1", [data])
            row = cursor.fetchone()
        if row is not None:
            raise forms.ValidationError("Nomor STR sudah terdaftar")
        return data

    def save(self):
        super().save()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        no_str = self.cleaned_data.get('nomor_str')
        nama = self.cleaned_data.get('nama')
        no_hp = self.cleaned_data.get('no_hp')
        gelar_depan = self.cleaned_data.get('gelar_depan')
        gelar_belakang = self.cleaned_data.get('gelar_belakang')
        peran_pengguna = 'Dokter'

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO siruco.AKUN_PENGGUNA VALUES(%s,%s,%s)",
                           [username, password, peran_pengguna])
            cursor.execute("INSERT INTO siruco.ADMIN VALUES(%s)", [username])
            cursor.execute("INSERT INTO siruco.DOKTER VALUES(%s,%s,%s,%s,%s,%s)", 
            [username, no_str, nama, no_hp, gelar_depan, gelar_belakang])

class RegisterAdminForm(RegisterUserForm):
    def save(self):
        super().save()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        nama = "Admin"
        peran_pengguna = 'Admin'
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO siruco.AKUN_PENGGUNA VALUES(%s,%s,%s)",
                           [username, password, peran_pengguna])
            cursor.execute("INSERT INTO siruco.ADMIN VALUES(%s)", [username])

class RegisterPenggunaPublik(RegisterUserForm):
    nik = forms.CharField( 
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=20)
    
    nama = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50)
    
    nohp = forms.CharField(
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=12)

    def save(self):
        super().save()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        nik = self.cleaned_data.get('nik')
        nama = self.cleaned_data.get('nama')
        status = "Aktif"
        nohp = self.cleaned_data.get('nohp')
        peran_pengguna = "Pengguna Publik"
        with connection.cursor() as cursor:
            # cursor.execute("SELECT * FROM PENGGUNA_PUBLIK ORDER BY id_konsumen DESC LIMIT 1")
            # new_id_number = int(cursor.fetchone()[0][3:]) + 1
            # new_id = f"FKC{new_id_number:07}"
            cursor.execute("INSERT INTO siruco.AKUN_PENGGUNA VALUES(%s,%s,%s)",
                           [username, password, peran_pengguna])
            cursor.execute("INSERT INTO siruco.PENGGUNA_PUBLIK VALUES(%s,%s,%s,%s,%s,%s)",
                           [username, nik, nama, status, peran_pengguna, nohp])

