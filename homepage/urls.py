from django.urls import path
from homepage.views import *

urlpatterns = [
    path('profile/', profile, name="profile"),
    path('register/pengguna_publik/', register_pengguna_publik, name="register_pengguna_publik"),
    path('register/admin_satgas/', register_admin_satgas, name="register_admin_satgas"),
    path('register/dokter/', register_dokter, name="register_dokter"),
    path('register/admin/', register_admin, name="register_admin"),
    path('register/', register, name="register"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),
    path('', home, name="home")
]
