from django.db import connection
from django.shortcuts import render, redirect
from homepage.forms import *
from django.contrib import messages

def home(request):
    return render(request, "index.html")

def no_user(function):
    def wrapper(request, *args, **kwargs):
        username = request.session.get('username')
        nama = request.session.get('nama')
        if username is not None:
            return redirect('/profile/')
        else:
            return function(request, *args, **kwargs)
    return wrapper


def check_role(username):
    with connection.cursor() as cursor:
        for i in ["PENGGUNA_PUBLIK", "ADMIN_SATGAS", "DOKTER", "ADMIN"]:
            cursor.execute(
                f"SELECT * FROM siruco.{i} WHERE username=%s LIMIT 1;",
                [username])
            if cursor.fetchone() is not None:
                return i

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        username = request.session.get('username')
        if username is None:
            return redirect('/login/')
        else:
            return function(request, *args, **kwargs)
    return wrapper


def get_role_data(username, role):
    print(role)
    with connection.cursor() as cursor:
        cursor.execute(
            f"SELECT * FROM siruco.{role} WHERE username=%s LIMIT 1;",
            [username])
        return cursor.fetchone()

def input_session(request, username, nama):
    request.session['username'] = username
    request.session['nama'] = nama
    request.session['role'] = check_role(username)

@no_user
def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        try:
            if form.is_valid():
                    with connection.cursor() as cursor:
                        cursor.execute(
                            "SELECT * FROM siruco.AKUN_PENGGUNA WHERE username=%s;",
                            [form.cleaned_data.get('username')])
                        data = cursor.fetchone()
                    input_session(request, form.cleaned_data.get('username'), data[0])
                    return redirect('/profile/')
        except:
            messages.warning(request, 'Username salah')
    elif request.method == 'GET':
        form = LoginForm()
    context = {'form': form}
    return render(request, "login.html", context)


def logout(request):
    request.session.flush()
    return redirect('/')


@no_user
def register(request):
    if request.method == 'POST':
        form = RolePickerForm(request.POST)
        if form.is_valid():
            url_map = {'PENGGUNA_PUBLIK': '/register/pengguna_publik/',
                       'ADMIN_SATGAS': '/register/admin_satgas/',
                       'DOKTER': '/register/dokter/',
                       'ADMIN': '/register/admin/'}
            return redirect(url_map[form.cleaned_data.get('peran')])
    elif request.method == 'GET':
        form = RolePickerForm()
    context = {'form': form}
    return render(request, "register.html", context)

@no_user
def register_pengguna_publik(request):
    if request.method == 'POST':
        form1 = RegisterPenggunaPublik(request.POST)
        if form1.is_valid():
            try:
                form1.save()
                nama = form1.cleaned_data.get('nama')
                input_session(request, form1.cleaned_data.get('username'), nama)
                return redirect('/profile/')
            except:
                return render(request, 'register_pengguna_publik.html', {'form1': form1, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})
    elif request.method == 'GET':
        form1 = RegisterPenggunaPublik()
    context = {'form1': form1}
    return render(request, "register_pengguna_publik.html", context)

@no_user
def register_admin_satgas(request):
    if request.method == 'POST':
        form2 = RegisterAdminSatgasForm(request.POST)
        if form2.is_valid():
            try:
                form2.save()
                nama = 'Admin'
                input_session(request, form2.cleaned_data.get('username'), nama)
                return redirect('/profile/')
            except:
                return render(request, 'register_admin_satgas.html', {'form2': form2, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})
    elif request.method == 'GET':
        form2 = RegisterAdminSatgasForm()
    context = {'form': form2}
    return render(request, "register_admin_satgas.html", context)


@no_user
def register_dokter(request):
    if request.method == 'POST':
        form = RegisterDokterForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                nama = form.cleaned_data.get('nama')
                input_session(request, form.cleaned_data.get('username'), nama)
                return redirect('/profile/')
            except:
                return render(request, 'register_dokter.html', {'form': form, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})
    elif request.method == 'GET':
        form = RegisterDokterForm()
    context = {'form': form}
    return render(request, "register_dokter.html", context)


@no_user
def register_admin(request):
    if request.method == 'POST':
        form = RegisterAdminForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                nama = 'Admin'
                input_session(request, form.cleaned_data.get('username'), nama)
                return redirect('/profile/')
            except:
                return render(request, 'register_admin.html', {'form': form, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})
    elif request.method == 'GET':
        form = RegisterAdminForm()
    context = {'form': form}
    return render(request, "register_admin.html", context)

@user_login_required
def profile(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM siruco.AKUN_PENGGUNA WHERE username=%s",
                       [request.session.get("username")])
        user_data = cursor.fetchone()
        role_data = get_role_data(
            request.session.get("username"),
            request.session.get("role"))
    context = {'user': user_data, 'role': role_data}
    return render(request, "profile.html", context)
