from django.db import connection
from django.shortcuts import render, redirect
from homepage.views import user_login_required
from .forms import *
from django.http import JsonResponse


# Create your views here.
def index(request):
    context = {}
    return render(request, "jingga.html", context)

def cursor_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@user_login_required
def listruanganhotel(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.HOTEL_ROOM")
        data = cursor_fetchall(cursor)
        print(data)
        
    with connection.cursor() as cursor: 
        cursor.execute("SELECT kodehotel,koderoom FROM SIRUCO.HOTEL_ROOM EXCEPT (SELECT kodehotel,koderoom from siruco.reservasi_hotel)")
        notreserved = cursor_fetchall(cursor)
    
    with connection.cursor() as cursor:
        cursor.execute("SELECT kodehotel,koderoom FROM SIRUCO.RESERVASI_HOTEL WHERE tglkeluar< current_date")
        finished = cursor_fetchall(cursor)

    context = {'data': data, 'notreserved':notreserved, 'finished':finished}
    return render(request, "READ-Listruanganhotel.html", context)

@user_login_required
def createlistruanganhotel(request):
    if request.session.get("role") != "ADMIN":
        return redirect('/listruanganhotel')
    if request.method == 'POST':
        form = CreateHotelRoomForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/listruanganhotel")
    elif request.method == 'GET':
        form = CreateHotelRoomForm()
    context = {'form': form}
    return render(request, "CREATE-ListRuanganHotel.html", context)

def load_kodehotel(request):
    kodehotel = request.GET.get('kodehotel') 
    noRoom = 0
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT MAX(CAST(SUBSTRING(koderoom,3,3) AS INT)) FROM SIRUCO.HOTEL_ROOM WHERE koderoom LIKE '%RH%' AND kodehotel = '"+kodehotel+"'"
        )
        koderoom = cursor.fetchall()
        if koderoom[0][0] == None:
            insert_noRoom = "RH001"
        else:
            for i in koderoom:
                noRoom = i[0] + 1
            if (noRoom < 10):
                insert_noRoom = "RH00" + str(noRoom)
            elif noRoom < 100:
                insert_noRoom = "RH0" + str(noRoom)
            elif noRoom < 1000:
                insert_noRoom = "RH" + str(noRoom)
    return JsonResponse({'insert_noRoom':insert_noRoom},status=200)

@user_login_required
def updateruanganhotel(request):
    if request.session.get("role") != "ADMIN":
        return redirect('/listruanganhotel')
    
    if request.method == "GET":
        kodehotel = request.GET.get('kodehotel')
        koderoom = request.GET.get('koderoom')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.HOTEL_ROOM WHERE KODEHOTEL = %s AND KODEROOM = %s ",
            [kodehotel,koderoom])
            val = cursor.fetchone()
            print(val)
            key = [col[0] for col in cursor.description]
            print(key)
            data_ruangan = dict(zip(key, val))
        form = updatedataruangan(initial=data_ruangan)
    elif request.method == 'POST':
        form = updatedataruangan(request.POST)
        if form.is_valid():
            form.save_update()
            return redirect('/listruanganhotel')

    context = {'form':form}
    return render(request,'UPDATE-Ruanganhotel.html',context) 

@user_login_required
def deleteruanganhotel(request):
    cursor = connection.cursor()
    if request.session.get("role") != "ADMIN":
        return redirect('/listruanganhotel')

    if request.method == "GET":
        kodehotel = request.GET.get('kodehotel')
        koderoom = request.GET.get('koderoom')
    
        if None in [kodehotel,koderoom]:
            return redirect('/listruanganhotel')

        else:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.HOTEL_ROOM WHERE KODEHOTEL = %s AND KODEROOM = %s ",
                [kodehotel,koderoom])
            return redirect('/listruanganhotel')

@user_login_required
def listreservasihotel(request):
    print(request.session.get("role"))
    if (request.session.get("role") == "ADMIN_SATGAS") or (request.session.get("role") == "PENGGUNA_PUBLIK"):
        if request.method == "GET":
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.PASIEN JOIN SIRUCO.RESERVASI_HOTEL ON KODEPASIEN = NIK")
                data_reservasi_hotel = cursor_fetchall(cursor)
            
            with connection.cursor() as cursor:
                cursor.execute("SELECT kodepasien,tglmasuk FROM SIRUCO.RESERVASI_HOTEL WHERE tglmasuk = null")
                datenotexist = cursor_fetchall(cursor)

            context = {'data': data_reservasi_hotel, 'deleteable':datenotexist}
            return render(request, "READ-Listreservasihotel.html", context)
    else: return redirect('/') 

@user_login_required
def createreservasihotel(request):
    if request.session.get("role") == "ADMIN_SATGAS" or request.session.get("role") == "PENGGUNA_PUBLIK":
        if request.method == 'POST':
            form = CreateReservasiHotelForm(request.POST)
            sem = request.POST.get('koderoom')
            form.fields['koderoom'].choices = [(sem,sem)]
            if form.is_valid():
                form.save()
                return redirect("/listreservasihotel")
        elif request.method == 'GET':
            form = CreateReservasiHotelForm()
        context = {'form': form}
        return render(request, "CREATE-Reservasihotel.html", context)
    else: return redirect('/listreservasihotel')
    
@user_login_required
def load_semuakodehotel(request):
    kodehotel = request.GET.get('kodehotel')
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT koderoom FROM SIRUCO.HOTEL_ROOM WHERE kodehotel = '"+kodehotel+"'"
        )
        koderoom = cursor_fetchall(cursor)
    return render(request,'hr/koderoom_options.html',{'koderoom':koderoom})

@user_login_required
def updatereservasihotel(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/listreservasihotel')
    
    if request.method == "GET":
        kodepasien = request.GET.get('kodepasien')
        tglmasuk = request.GET.get('tglmasuk').replace(".", "")
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RESERVASI_HOTEL WHERE kodepasien = %s AND tglmasuk = %s ",
            [kodepasien,tglmasuk])
            val = cursor.fetchone()
            key = [col[0] for col in cursor.description]
            data_reservasihotel = dict(zip(key, val))
            print(data_reservasihotel)                                                                                                                  
        form = UpdateDataReservasiHotel(initial=data_reservasihotel)
    elif request.method == 'POST':
        form = UpdateDataReservasiHotel(request.POST)
        if form.is_valid():
            form.save_update()
            return redirect('/listreservasihotel')

    context = {'form':form}
    return render(request,'UPDATE-Reservasihotel.html',context) 

@user_login_required
def deletereservasihotel(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/listreservasihotel')

    if request.method == "GET":
        kodepasien = request.GET.get('kodepasien')
        tglmasuk = request.GET.get('tglmasuk').replace(".","")
    
        if None in [kodepasien,tglmasuk]:
            print('gagallllllllllllllll')
            return redirect('/listreservasihotel')
        else:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.TRANSAKSI_HOTEL WHERE kodepasien = %s AND tglmasuk = %s ",
                [kodepasien,tglmasuk])
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.TRANSAKSI_BOOKING WHERE kodepasien = %s AND tglmasuk = %s ",
                [kodepasien,tglmasuk])
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM SIRUCO.RESERVASI_HOTEL WHERE kodepasien = %s AND tglmasuk = %s ",
                [kodepasien,tglmasuk])
            return redirect('/listreservasihotel')

@user_login_required
def listtransaksihotel(request):
    if (request.session.get("role") == "ADMIN_SATGAS"):
        if request.method == "GET":
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_HOTEL")
                data_transaksi_hotel = cursor_fetchall(cursor)

            context = {'data': data_transaksi_hotel}
            return render(request, "READ-Listtransaksihotel.html", context)
    else: return redirect('/')

@user_login_required
def listtransaksibooking(request):
    if (request.session.get("role") == "ADMIN_SATGAS") or (request.session.get("role") == "PENGGUNA_PUBLIK"):
        if request.method == "GET":
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.PASIEN JOIN SIRUCO.TRANSAKSI_BOOKING on nik = kodepasien")
                data_transaksi_booking = cursor_fetchall(cursor)

            context = {'data': data_transaksi_booking}
            return render(request, "READ-Listtransaksibooking.html", context)
    else: return redirect('/')

@user_login_required
def updatetransaksihotel(request):
    if request.session.get("role") != "ADMIN_SATGAS":
        return redirect('/listtransaksihotel')
    
    if request.method == "GET":
        idtransaksi = request.GET.get('idtransaksi')
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_HOTEL WHERE idtransaksi = %s",
            [idtransaksi])
            val = cursor.fetchone()
            key = [col[0] for col in cursor.description]
            data_transaksihotel = dict(zip(key, val))
            print(data_transaksihotel)                                                                                                                  
        form = UpdateDataTransaksiHotel(initial=data_transaksihotel)
    elif request.method == 'POST':
        form = UpdateDataTransaksiHotel(request.POST)
        if form.is_valid():
            form.save_update()
            return redirect('/listtransaksihotel')

    context = {'form':form}
    return render(request,'UPDATE-Transaksihotel.html',context) 