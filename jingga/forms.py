from django import forms
from django.db import connection, transaction, IntegrityError, InternalError
from datetime import datetime
from django.core.exceptions import ValidationError

class CreateHotelRoomForm(forms.Form):
    cursor = connection.cursor()
    KODE_HOTEL = []
    JENIS_BED = [('King','King Bed'),('TwinSingle','Twin Single Bed')]
    TIPE_ROOM = [('Deluxe','Deluxe'),('Premium','Premium'),('Suite','Suite')]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT kode, kode FROM SIRUCO.HOTEL")
            self.fields['kodehotel'].choices = cursor.fetchall()

    kodehotel = forms.ChoiceField(choices=KODE_HOTEL,
                                  widget=forms.Select(attrs={'class': 'form-control'}))

    koderoom = forms.CharField(widget = forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    jenisbed = forms.ChoiceField(choices=JENIS_BED,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    tiperoom = forms.ChoiceField(choices=TIPE_ROOM,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    harga = forms.IntegerField(
                        widget=forms.NumberInput(attrs={'class': 'form-control'}))

    def save(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO SIRUCO.HOTEL_ROOM VALUES(%s, %s, %s, %s, %s)",
                               [cd.get("kodehotel"), cd.get('koderoom'), cd.get("jenisbed"),
                                cd.get("tiperoom"), cd.get("harga")])
        except IntegrityError:
            pass

    def clean(self):
        kodehotel = self.cleaned_data.get("kodehotel")
        koderoom = self.cleaned_data.get("koderoom")
        with connection.cursor() as cursor:
            cursor.execute("SELECT kodehotel, koderoom FROM SIRUCO.HOTEL_ROOM")
            existing_data = cursor.fetchall()
        if (kodehotel, koderoom) in existing_data:
            raise forms.ValidationError("Masukan data gagal")

class updatedataruangan(CreateHotelRoomForm):
    KODE_HOTEL = []
    JENIS_BED = [('King','King Bed'),('TwinSingle','Twin Single Bed')]
    TIPE_ROOM = [('Deluxe','Deluxe'),('Premium','Premium'),('Suite','Suite')]

    kodehotel = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    koderoom = forms.CharField(widget = forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    jenisbed = forms.ChoiceField(choices=JENIS_BED,widget=forms.Select(attrs={'class': 'form-control'}))

    tiperoom = forms.ChoiceField(choices=TIPE_ROOM,widget=forms.Select(attrs={'class': 'form-control'}))
    
    harga = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    def save_update(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                with transaction.atomic():
                    cursor.execute("""
                    UPDATE SIRUCO.HOTEL_ROOM SET jenisbed = %s, tipe = %s, harga = %s WHERE kodehotel=%s AND koderoom=%s""",
                            [cd.get("jenisbed"), cd.get("tiperoom"),
                            cd.get("harga"), cd.get("kodehotel"),
                            cd.get("koderoom")])
        except IntegrityError:
            pass

class CreateReservasiHotelForm(forms.Form):

    cursor = connection.cursor()
    
    
    cursor.execute("SELECT NIK FROM siruco.PASIEN")
    NIK_PASIEN = [(row[0],row[0]) for row in  cursor.fetchall()]
    nik = forms.ChoiceField(choices=NIK_PASIEN,
        widget=forms.Select(attrs={'class': 'form-control'}))

    tglmasuk = forms.DateField(
        widget=forms.SelectDateWidget,
        initial=datetime.now())

    tglkeluar = forms.DateField(
        widget=forms.SelectDateWidget,
        initial=datetime.now())

    cursor.execute("SELECT kode FROM SIRUCO.HOTEL")
    KODE_HOTEL = [(row[0],row[0]) for row in cursor.fetchall()]
    kodehotel = forms.ChoiceField(choices=KODE_HOTEL,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    
    kodehotelbersih = ""
    cursor.execute("SELECT koderoom FROM SIRUCO.HOTEL_ROOM WHERE kodehotel = %s", [kodehotelbersih])
    KODE_ROOM = [(row[0],row[0]) for row in cursor.fetchall()]
    koderoom = forms.ChoiceField(choices=KODE_ROOM,
                                  widget=forms.Select(attrs={'class': 'form-control'}),required=True)

    def save(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO SIRUCO.RESERVASI_HOTEL VALUES(%s, %s, %s, %s, %s)",
                               [cd.get("nik"), cd.get('tglmasuk'), cd.get('tglkeluar'),
                                cd.get("kodehotel"), cd.get("koderoom")])
        except IntegrityError:
            pass

    def clean(self):
        cd = self.cleaned_data
        kodepasien = cd.get("nik")
        tglmsk = cd.get("tglmasuk")
        tglklr = cd.get("tglkeluar")
        databersih = super().clean()

        if tglklr < tglmsk:
            raise forms.ValidationError("Tanggal keluar harus setelah tanggal masuk")
        with connection.cursor() as cursor:
            cursor.execute("SELECT kodepasien, tglmasuk FROM SIRUCO.RESERVASI_HOTEL")
            existing_data = cursor.fetchall()
        if (kodepasien, tglmsk) in existing_data:
            raise forms.ValidationError("Masukan data gagal")
        else: 
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO SIRUCO.RESERVASI_HOTEL VALUES(%s, %s, %s, %s, %s)",
                               [cd.get("nik"), cd.get('tglmasuk'), cd.get('tglkeluar'),
                                cd.get("kodehotel"), cd.get("koderoom")])

class UpdateDataReservasiHotel(forms.Form):
    kodepasien = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    tglmasuk = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    tglkeluar = forms.DateField(widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))

    kodehotel = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))
    
    koderoom = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    def save_update(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                with transaction.atomic():
                    cursor.execute("""
                    UPDATE SIRUCO.RESERVASI_HOTEL SET tglkeluar = %s WHERE kodepasien=%s AND tglmasuk=%s""",
                            [cd.get("tglkeluar"),cd.get("kodepasien"),cd.get("tglmasuk")])
        except IntegrityError:
            pass

    def clean(self):
        cd = self.cleaned_data
        databersih = super().clean()
        tanggalmasuk = databersih['tglmasuk']
        tanggalkeluar = databersih['tglkeluar']
        if str(tanggalkeluar) < tanggalmasuk:
            raise forms.ValidationError("Tanggal keluar harus setelah tanggal masuk")
        kodepasien = cd.get("kodepasien")
        with connection.cursor() as cursor:
            cursor.execute("SELECT kodepasien, tglmasuk FROM SIRUCO.RESERVASI_HOTEL")
            existing_data = cursor.fetchall()
        if (kodepasien, tanggalmasuk) in existing_data:
            raise forms.ValidationError("Masukan data gagal")

class UpdateDataTransaksiHotel(forms.Form):
    cursor = connection.cursor()

    idtransaksi = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    kodepasien = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))

    tanggalpembayaran = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}),required=False)

    waktupembayaran = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}),required=False)
    
    totalbayar = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly'}),required=False)

    statusbayar = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def save_update(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                with transaction.atomic():
                    cursor.execute("""
                    UPDATE SIRUCO.TRANSAKSI_HOTEL SET statusbayar = %s WHERE idtransaksi = %s""",
                            [cd.get("statusbayar"),cd.get("idtransaksi")])
        except IntegrityError:
            pass

    def clean(self):
        cd = self.cleaned_data
        idtransaksi = cd.get("idtransaksi")
        with connection.cursor() as cursor:
            cursor.execute("SELECT idtransaksi FROM SIRUCO.TRANSAKSI_HOTEL")
            existing_data = cursor.fetchall()
        if (idtransaksi) in existing_data:
            raise forms.ValidationError("Masukan data gagal")
        if len(cd.get("statusbayar")) > 15:
            raise forms.ValidationError("Status Pembayaran Terlalu Panjang")

