from django.urls import path
from jingga.views import *

app_name = 'jingga'
urlpatterns = [
    path('', index, name="index"),
    path('listruanganhotel', listruanganhotel, name="listruanganhotel"),
    path('create-listruanganhotel', createlistruanganhotel, name="create-listruanganhotel"),
    path('ajax/load-kodehotel/', load_kodehotel, name='ajax-load-kodehotel'),
    path('update-ruanganhotel/', updateruanganhotel, name="update-ruanganhotel"),
    path('delete-ruanganhotel/', deleteruanganhotel, name="delete-ruanganhotel"),
    path('listreservasihotel/', listreservasihotel, name="listreservasihotel"),
    path('create-reservasihotel/', createreservasihotel, name="create-reservasihotel"),
    path('ajax/load-semuakodehotel/', load_semuakodehotel, name='ajax-load-semuakodehotel'),
    path('update-reservasihotel/', updatereservasihotel, name="update-reservasihotel"),
    path('delete-reservasihotel/', deletereservasihotel, name="delete-reservasihotel"),
    path('listtransaksihotel', listtransaksihotel, name="listtransaksihotel"),
    path('update-transaksihotel/', updatetransaksihotel, name="update-transaksihotel"),
    path('listtransaksibooking', listtransaksibooking, name="listtransaksibooking"),
]
